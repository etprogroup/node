var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var methodOverride = require('method-override');
var cors = require('cors');

var CronJob = require('cron').CronJob;

var libs = process.cwd() + '/libs/';
require(libs + 'auth/auth');

var config = require('./config');
var log = require('./log')(module);
var oauth2 = require('./auth/oauth2');
var phoneCode = require('./auth/phoneCode');
var facebook = require('./auth/facebook');
var phoneNumber = require('./auth/phoneNumber');

var Notifications = require(libs + 'modules/notifications');

var api = require('./routes/api');
var login = require('./routes/login');
var users = require('./routes/users');
var appointments = require('./routes/appointments');
var shops = require('./routes/shops');
var services = require('./routes/services');
var products = require('./routes/products');
var employees = require('./routes/employees');
var customers = require('./routes/customers');
var profiles = require('./routes/profiles');
var images = require('./routes/images');
var brands = require('./routes/brands');
var orders = require('./routes/orders');
var payments = require('./routes/payments');
var posts = require('./routes/posts');
var contents = require('./routes/contents');
var tills = require('./routes/tills');
var statistics = require('./routes/statistics');
var recommended = require('./routes/recommended');
var tableUsers = require('./routes/tableUsers');
var chatbots = require('./routes/chatbots');
var chats = require('./routes/chats');
var requests = require('./routes/requests');
var contacts = require('./routes/contacts');

var app = express();

app.use(cors())
app.use(bodyParser.json({limit:'10mb'}));
app.use(bodyParser.urlencoded({extended:true, limit:'10mb'}));
app.use(cookieParser());
app.use(methodOverride());
app.use(passport.initialize());

app.use('/', api);
app.use('/api', api);
app.use('/api/login', login);
app.use('/api/oauth/token', oauth2.token);
app.use('/api/oauth/sms/token', phoneCode.token);
app.use('/api/oauth/facebook', facebook);
app.use('/api/oauth/phone-number', phoneNumber);
app.use('/api/appointments', appointments);
app.use('/api/shops', shops);
app.use('/api/users', users);
app.use('/api/services', services);
app.use('/api/products', products);
app.use('/api/employees', employees);
app.use('/api/customers', customers);
app.use('/api/profiles', profiles);
app.use('/api/image-upload', images);
app.use('/api/brands', brands);
app.use('/api/orders', orders);
app.use('/api/payments', payments);
app.use('/api/posts', posts);
app.use('/api/contents', contents);
app.use('/api/tills', tills);
app.use('/api/statistics', statistics);
app.use('/api/recommended', recommended);
app.use('/api/tableUsers', tableUsers);
app.use('/api/chatbots', chatbots);
app.use('/api/chat', chats);
app.use('/api/requests', requests);
app.use('/api/contacts', contacts);


// launch cron job with daily appointment reminders
let cron= config.get('cron'); 
if (cron) {
    var job = new CronJob(cron, function() {
        Notifications.cron();
    }, null, false, 'Europe/Madrid');

    job.start();
}

let closeTill= config.get('closeTill');
if (closeTill) {
    var jobCloseTill = new CronJob(closeTill, function() {
        Notifications.closeTill();
    }, null, false, 'Europe/Madrid');

    jobCloseTill.start();
}

// catch 404 and forward to error handler
app.use(function(req, res, next){
    res.status(404);
    log.debug('%s %d %s', req.method, res.statusCode, req.url);
    res.json({ 
    	error: 'Not found' 
    });
    return;
});

// error handlers
app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('%s %d %s', req.method, res.statusCode, err.message);
    res.json({ 
    	error: err.message 
    });
    return;
});

module.exports = app;