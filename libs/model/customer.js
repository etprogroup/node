﻿var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var crypto = require('crypto');

var CustomerSchema = new Schema({
    email:{
        type:String
    },
    phoneNumber:{
        type:String
    },
    name: {
        type: String
    },
    surname: {
        type: String,
    },
    image: {
        type: String,
    },
    sex: {
        type: String,
        enum: ['Hombre', 'Mujer']
    },
    birthday: {
        type: Date
    },
    personalPhoto: String,
    language: {
        type: String
    },
    hashedPassword: {
        type: String
    },
    salt: {
        type: String,
        default: 'salt'
    },
    loginType: [{
        type: String,
        enum: [null,'Local', 'Google','Facebook']
    }],
    hairType: {
        type: String,
        enum: [null,'Fino', 'Medio','Grueso']
    },
    hairTexture: {
        type: String,
        enum: [null,'Liso', 'Ondulado','Rizado']
    },
    hairColor: {
        type: String,
        enum: [null,'Rubio', 'Rojo/Cobrizo','Castano/Negro']
    },
    hairState: {
        type: String,
        enum: [null,'Virgen', 'Danado']
    },
    nailType: {
        type: String,
        enum: [null,'Naturales', 'Postizas','Debil','Fuerte']
    },
    nailPolish: {
        type: String,
        enum: [null,'Normal', 'Semipermanente']
    },
    skinType: {
        type: String,
        enum: [null,'Grasa', 'Seca','Mixta']
    },
    eyebrowType: {
        type: String,
        enum: [null,'Pobladas', 'Despobladas','Claras','Oscuras','Naturales','Tatuadas','Microbladding','MicropigmentaciÃ³n','Tinte']
    },
    eyelashType: {
        type: String,
        enum: [null,'Pobladas', 'Cortas','Largas','Rizadas','Rectas','Finas','Gruesas']
    },
    favouritesProducts:[{type: Schema.ObjectId, ref: 'Product'}],
    favouritesServices:[{type: Schema.ObjectId, ref: 'Service'}],
    shop: { type: Schema.ObjectId, ref: 'Shop' },
    pictures: [String],
    shippingAddress:[{
        address:String,
        city:String,
        state:String,
        postalCode:Number
    }],
    billingAddress:[{
        address:String,
        city:String,
        state:String,
        postalCode:Number
    }],
    cardLastNumbers:String,
    cardSaved:{
        type: String,
        default:'false'
    },
    payerSaved:{
        type: String,
        default:'false'
    },
    app:{
        platform:String,
        created:{
            type:Date,
            default: Date.now
        }
    },
    FCMToken:String,
    created: {
        type: Date,
        default: Date.now
    },
    facebook: {
        type: String,
    },
    facebookToken: {
        type: String,
    },
    facebookId: {
        type: String,
    },
    // track creation and modification hanges
    createdDate : {
        type: Date,
        default: Date.now
    },
    createdBy: {type: Schema.ObjectId, ref: 'employee'},
    createdDetails: String,
    nif: { type: String },
    // track updated
    lastUpdatedDate: Date,
    lastUpdatedBy: {type: Schema.ObjectId, ref: 'employee'},
    lastUpdatedDetails: String
});


var autoPopulate = function(next) {
    this.populate('shop');
    this.populate('favouritesProducts');
    next();
};

CustomerSchema.pre('findById', autoPopulate);
CustomerSchema.pre('find', autoPopulate);

module.exports = mongoose.model('Customer', CustomerSchema);
