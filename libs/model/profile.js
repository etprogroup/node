var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

	var ProfileSchema = new Schema({
		name:{
			type:String,
			required:true,
		},
		hairType: [{
			type: String,
			enum: ['Fino', 'Medio','Grueso']
		}],
		hairTexture: [{
			type: String,
			enum: ['Liso', 'Ondulado','Rizado']
		}],
		hairColor: [{
			type: String,
			enum: ['Rubio', 'Rojo/Cobrizo', 'Castano/Negro']
		}],
		hairState: [{
			type: String,
			enum: ['Virgen', 'Danado']
		}],
		nailType: [{
			type: String,
			enum: ['Naturales', 'Postizas','Debil', 'Fuerte']
		}],
		nailPolish: [{
			type: String,
			enum: ['Normal', 'Semipermanente']
		}],
		skinType: [{
			type: String,
			enum: ['Grasa', 'Seca','Mixta']
		}],
		eyebrowType: [{
			type: String,
			enum: ['Pobladas', 'Despobladas', 'Claras', 'Oscuras', 'Naturales', 'Tatuadas', 'Microbladding', 'Micropigmentacion', 'Tinte']
		}],
		eyelashType: [{
			type: String,
			enum: ['Pobladas', 'Cortas','Largas','Rizadas','Rectas','Finas','Gruesas']
		}],
		created: {
			type: Date,
			default: Date.now
		}

	});

module.exports = mongoose.model('Profile', ProfileSchema);
