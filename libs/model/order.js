var mongoose = require('mongoose'),
		Schema = mongoose.Schema;

var Order = new Schema({
    source : {
        type: String,
        enum: ['Shop', 'App', 'Web','Facebook'],
        default: 'Shop'
    },
	customer: { type: Schema.ObjectId, ref: 'Customer' },
	appointment: { type: Schema.ObjectId, ref: 'Appointment' },
	shop: { type: Schema.ObjectId, ref: 'Shop' },
    /* Possible values Service, Product, Service&Product*/
	orderType:{
		type:String
	},
	totalAmount:{
		type:Number
	},
	orderList:[{
		product: { type: Schema.ObjectId, ref: 'Product' },
		service: { type: Schema.ObjectId, ref: 'Service' },
		quantity:Number,
		price:Number,
		discount:Number
	}],
	orderStatusChange:[{
		status:String,
		quantity:Number,
		image: String,
		created: {
			type: Date,
			default: Date.now
		},
		comments:String,
        paymentMethod:[{ 
				typePayment: String,
				valuePayment: Number
			}]
	}],
	created: {
		type: Date,
		default: Date.now
	},
	lastModified: {
		type: Date,
		default: Date.now
	},
	photoPublishAuth: {
		type: Boolean,
		default: true
	}
});

var autoPopulate = function(next) {
	this.populate('orderList.product');
	this.populate('orderList.service');
	this.populate('shop',['_id','settings.name']);
	this.populate('customer',['_id','name','surname']);
    this.populate('appointment');
	next();
};

Order.pre('findById', autoPopulate);
Order.pre('find', autoPopulate);

module.exports = mongoose.model('Order', Order);
