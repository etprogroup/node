var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

	Product = new Schema({
        sku:{
            type:String
        },
        barCode:{
            type:String
        },
		name:{
			type:String,
			required:true,
			unique:true
		},
		brand: { type: Schema.ObjectId, ref: 'Brand' },
		description:{
			type:String
		},
		large_description:{
			type:String
		},
		line:String,
		category:String,
		price: {
			type: Number,
			default: 0
		},
		public: {
			type: Boolean,
			default: true
		},
		size: {
			type: String
		},
		stock:{
			type: Number,
			default: 0
		},
        minimum_stock:{
            type: Number,
            default: 0
        },
        purchase_price:{
            type: Number,
            default: 0
        },
		stockList: [{
			amount: Number,
			date: Date,
			user: { type: Schema.ObjectId, ref: 'Employee' }
		}],
		salePrice: {
			type: Number,
			default: 0
		},
		sex: [{
			type: String,
			enum: ['Hombre', 'Mujer'],
			required: true
		}],
        format: {
            type: String
        },
		images:[{
			type: String
		}],
		videos:[{
			type: String
		}],
		shop: { type: Schema.ObjectId, ref: 'Shop' },
		salonType:[{
			type: String,
			enum: ['--', '---','-----'],
			required: true
		}],
		profile: [{
			type: String
		}],
		created: {
			type: Date,
			default: Date.now
		}

	});

var autoPopulate = function(next) {
	this.populate('brand');
	next();
};

Product.pre('findById', autoPopulate);
Product.pre('find', autoPopulate);

module.exports = mongoose.model('Product', Product);
