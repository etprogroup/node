var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Appointment = new Schema({
    source : {
        type: String,
        enum: ['Shop', 'App', 'Web','Facebook'],
        default: 'Shop'
    },
    campaign: {
        type: String,
    },
    anonymousCustomer: {
			type: Boolean,
			default: false
		},
    customer: { type: Schema.ObjectId, ref: 'Customer'},
    services: [{ type: Schema.ObjectId, ref: 'Service' }],
    products: [ { type: Schema.ObjectId, ref: 'Product' } ],
    recommended: [{ type: Schema.ObjectId, ref:'Product'}],
    employee: { type: Schema.ObjectId, ref: 'Employee', required: true },
    shop : { type: Schema.ObjectId, ref: 'Shop', required: true},
    status: {
        type: String,
        enum: ['Pendiente', 'Confirmado', 'Cancelado', 'Pagado','Eliminado','Ocupado'],
        default: 'Confirmado'
    },
    parentAppointmentId:{ type: Schema.ObjectId, ref: 'Appointment'},
    substatus : {
        type: String,
        enum: ['Cancelado cliente', 'No apareció', 'TPV', 'Cash', 'App', 'Mixed']
    },
    observaciones: {
        type: String
    },
    totalPrice:{
        type: Number
    },
    technicalProductList:[{
        product: { type: Schema.ObjectId, ref: 'Product' },
        quantity:String,
        format:String
    }],
    // values:'Scheduler','Customer','Employee'
    createdBy:{
        type: String,
        default: 'Employee'
    },
    start: { type: String, required: true  },
    end: { type: String, required: true },
    images: [String],

    proposedServicesPrice: Number, //total value of the services or overriden by stylist
    proposedServicesPriceSentToClient: Date, //if the client was not NOTIFIED, this value is NULL
    
    // track creation and modification hanges
    createdDate : {
		type: Date,
		default: Date.now
	},

    checkboxFeedback: {
        type: Boolean,
        default: false
    },
    rating: {
        type: Number
    },
    reviewDetails: String,

    createdDetails: String,
    // track updated
    lastUpdatedDate: Date,
    lastUpdatedBy: {type: Schema.ObjectId, ref: 'employee'},
    lastUpdatedDetails: String,
    type:{
        type: String,
        default: 'appointment'
    }

});

var autoPopulate = function(next) {
  this.populate('shop');
  this.populate('customer');
  this.populate('employee');
  this.populate('products');
  this.populate('recommended');
  next();
};

Appointment.pre('findById', autoPopulate);
Appointment.pre('find', autoPopulate);

module.exports = mongoose.model('Appointment', Appointment);