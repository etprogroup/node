var mongoose = require('mongoose');
var Schema = mongoose.Schema;

	Service = new Schema({
		name:{
			type:String,
			required:true,
			unique:true
		},
		description:{
			type:String
		},
		price: {
			type: Number
		},
		priceFrom: {
			type: Boolean,
			default: false
		},
		salePrice: {
			type: Number,
		},
		sex: [{
			type: String,
			enum: ['Hombre', 'Mujer'],
		}],
		duration: {
			type: Number,
			default: 60
		},
		frequency: {
			type: Number,
			default: 60
		},
		confirmationRequired: {
			type: Boolean,
			default: false
		},
		images: [String],
		videos: [String],
		profile: [{
			type: String
		}],
		salonType:[{
			type: String,
			enum: ['Peluqueria', 'Estetica','Tattoo'],
			required: true
		}],
		shop: { type: Schema.ObjectId, ref: 'Shop' },
        count: Number,
		created: {
			type: Date,
			default: Date.now
		},
        public: {
            type: Boolean,
            default: true
        }
	});

var autoPopulate = function(next) {
  this.populate('shop');
  next();
};

Service.pre('findById', autoPopulate);
Service.pre('find', autoPopulate);


module.exports = mongoose.model('Service', Service);
