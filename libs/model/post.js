var mongoose = require('mongoose'),
		Schema = mongoose.Schema;
var crypto = require('crypto');


var Post_Status_Change = new Schema({
	old_status: {
		type: String,
		enum: ['Publish', 'Future', 'Draft', 'Pending', 'Private', 'Trash'],
		default: 'Draft'
	},
	employee: { type: Schema.ObjectId, ref: 'Employee' },
	created: {
		type: Date,
		default: Date.now
	}
})

var Post_Activity = new Schema({
	customer: { type: Schema.ObjectId, ref: 'Customer' },
	action: {
		type: String,
		enum: ['Read', 'Share', 'Comment', 'Like', 'Rate','Poll']
	},
	comment: String,
	rate: {
		type: Number,
		default: 0
	},
	liked: {
		type: Boolean,
		default: false
	},
	poll_answer: [String],
	created: {
		type: Date,
		default: Date.now
	}
})

var Post = new Schema({
	author:{
		type:String
	},
	content:{
		type:String
	},
	status: {
		type: String,
		enum: ['Publish', 'Future', 'Draft', 'Pending', 'Private', 'Trash'],
		default: 'Draft'
	},
	excerpt: {
		type: String
	},
	title: {
		type: String
	},
	push_message: {
		type: Boolean,
		default: false
	},
	target: {
		type: String,
		enum: ['Customers', 'Employees', 'Both'],
		default: 'Customers'
	},
    post_type: {
		type: String,
		enum: ['Post', 'Poll'],
		default: 'Post'
	},
	shop: { type: [Schema.ObjectId], ref: 'Shop' },
	mainPicture: String,
	pictures: [String],
	salonType:[{
		type: String,
		enum: ['Peluqueria', 'Estetica','Tattoo'],
		required: true
	}],
	profiles: [String],
	tags: [String],
	post_poll: {
		question: String,
		answers: [String]
	},
	post_status_changes: [Post_Status_Change],
	post_activity: [Post_Activity],
	location: {
		type: [Number],
		index: '2d'
	},

	lastModifiedBy: { type: Schema.ObjectId, ref: 'Employee' },
	lastModified:{
		type: Date,
		default: Date.now
	},

	createdBy: { type: Schema.ObjectId, ref: 'Employee' },
	created: {
		type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model('Post', Post);