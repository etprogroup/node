var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Chat = new Schema({

    sendby : {
        type: String,
        enum: ['employee', 'customer', 'system', 'shop'],
        default: 'system'
    },

    customer: { type: Schema.ObjectId, ref: 'Customer', required: true},
    employee: { type: Schema.ObjectId, ref: 'Employee', required: true},
    appointment: { type: Schema.ObjectId, ref: 'Appointment'},

    message: {
        type: String
    },

    time : {
        type: String, //Date,
        default: Date.now
    },

    update : {
        type: String, //Date,
        default: Date.now
    },

});

var autoPopulate = function(next){
  this.populate('customer');
  this.populate('employee');
  next();
};

Chat.pre('findById', autoPopulate);
Chat.pre('find', autoPopulate);

module.exports = mongoose.model('Chat', Chat);