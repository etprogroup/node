var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

var libs = process.cwd() + '/libs/';

var config = require(libs + 'config');

var Employee = require(libs + 'model/employee');
var Customer = require(libs + 'model/customer');
var Client = require(libs + 'model/client');
var AccessToken = require(libs + 'model/accessToken');

passport.use(new FacebookStrategy({
        clientID: '1426579824059865',
        clientSecret: '6c8886a2104a554243d67da45c99e70b',
        callbackURL: "http://dev.magnifique.me/api/oauth/facebook/callback",
        profileFields: ['id', 'emails', 'name']
    },
    function(accessToken, refreshToken, profile, done){
        process.nextTick(function () {
            return done(null, profile);
        });
    }

));


passport.use(new BasicStrategy(
    function(username, password, done) {
        Client.findOne({ clientId: username }, function(err, client) {
            if (err) { 
            	return done(err); 
            }

            if (!client) { 
            	return done(null, false); 
            }

            if (client.clientSecret !== password) { 
            	return done(null, false); 
            }

            return done(null, client);
        });
    }
));

passport.use(new ClientPasswordStrategy(
    function(clientId, clientSecret, done) {
        Client.findOne({ clientId: clientId }, function(err, client) {
            if (err) { 
            	return done(err); 
            }

            if (!client) { 
            	return done(null, false); 
            }

            if (client.clientSecret !== clientSecret) { 
            	return done(null, false); 
            }

            return done(null, client);
        });
    }
));

passport.use(new BearerStrategy(
    function(accessToken, done) {
        AccessToken.findOne({ token: accessToken }, function(err, token) {

            if (err) { 
            	return done(err); 
            }

            if (!token) { 
            	return done(null, false); 
            }
            if( Math.round((Date.now()-token.created)/1000) > config.get('security:tokenLife') ) {
                AccessToken.remove({ token: accessToken }, function (err) {
                    if (err) {
                    	return done(err);
                    } 
                });

                return done(null, false, { message: 'Token expired' });
            }
            Employee.findById(token.userId, function(err, user) {

                if (err) {
                	return done(err);
                }
                var responded=false;
                if (!user) {
                    Customer.findById(token.userId, function(err, user) {

                        if (err) {
                            return done(err);
                        }

                        if (!user) {
                            return done(null, false, { message: 'Unknown user' });
                        }

                        var info = { scope: '*' };
                        done(null, user, info);
                    });
                }else{
                    var info = { scope: '*' };
                    done(null, user, info);
                }
            });
        });
    }
));