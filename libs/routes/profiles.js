var express = require('express');
var passport = require('passport');
var router = express.Router();
var cors = require('cors');

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var Profile = require(libs + 'model/profile');

router.get('/', passport.authenticate('bearer', { session: false }), function(req, res) {

    Profile.find(function (err, profiles) {
        if (!err) {
            return res.json(profiles);
        } else {
            res.statusCode = 500;

            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.post('/', passport.authenticate('bearer', { session: false }), function(req, res) {

    var profile = new Profile({
        name : req.body.name,
        salonType : req.body.salonType,
        hairType : req.body.hairType,
        hairTexture : req.body.hairTexture,
        hairColor : req.body.hairColor,
        hairState : req.body.hairState,
        nailType : req.body.nailType,
        nailPolish : req.body.nailPolish,
        skinType : req.body.skinType,
        eyebrowType : req.body.eyebrowType,
        eyelashType : req.body.eyelashType
    });

    profile.save(function (err) {
        if (!err) {
            log.info("New profile created with id: %s", profile.name);
            return res.json({
                status: 'OK',
                profile: profile
            });
        } else {
            if(err.name === 'ValidationError') {
                res.statusCode = 400;
                res.json({
                    error: 'Profile Save validation error'
                });
            } else {
                res.statusCode = 500;

                log.error('Internal error(%d): %s', res.statusCode, err.message);

                res.json({
                    error: 'Profile Save Server error'
                });
            }
        }
    });
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    Profile.findById(req.params.id, function (err, profile) {

        if(!profile) {
            res.statusCode = 404;

            return res.json({
                error: 'Profile not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK',
                profile: profile
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});
router.options('/:id', cors());

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    var profileId = req.params.id;

    Profile.findById(profileId, function (err, profile) {
        if(!profile) {
            res.statusCode = 404;
            log.error('Profile with id: %s Not Found', profileId);
            return res.json({
                error: 'Not found'
            });
        }

        profile.name = req.body.name;
        profile.salonType = req.body.salonType;
        profile.hairType = req.body.hairType;
        profile.hairTexture = req.body.hairTexture;
        profile.hairColor = req.body.hairColor;
        profile.hairState = req.body.hairState;
        profile.nailType = req.body.nailType;
        profile.nailPolish = req.body.nailPolish;
        profile.skinType = req.body.skinType;
        profile.eyebrowType = req.body.eyebrowType;
        profile.eyelashType = req.body.eyelashType;

        profile.save(function (err) {
            if (!err) {
                log.info("Profile with id: %s updated", profileId);
                return res.json({
                    status: 'OK',
                    profile: profile
                });
            } else {
                if(err.name === 'ValidationError') {
                    res.statusCode = 400;
                    return res.json({
                        error: 'Validation error'
                    });
                } else {
                    res.statusCode = 500;

                    return res.json({
                        error: 'Server error'
                    });
                }
                log.error('Internal error (%d): %s', res.statusCode, err.message);
            }
        });
    });
});

router.delete('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    Profile.findByIdAndRemove(req.params.id, function (err, profile) {

        if(!profile) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK'
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

module.exports = router;
