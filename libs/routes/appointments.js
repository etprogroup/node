var express = require('express');
var passport = require('passport');
var router = express.Router();
var cors = require('cors');

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var Moment = require('moment');
var mongoose = require('mongoose');
var Appointment = require(libs + 'model/appointment');
var Shop = require(libs + 'model/shop');
var Notifications = require(libs + 'modules/notifications');
var Employee = require(libs + 'model/employee');
var Service = require(libs + 'model/service');
var Mailing = require(libs + 'modules/mailing');
var verifyAppointment = require(libs + 'modules/verifyAppointment');
var async = require('async');

router.get('/', passport.authenticate('bearer', { session: false }), function(req, res) {

    var search = { }
    if(req.query.date) search.start = { "$regex": req.query.date, "$options": "i" };

    Appointment.find( search, function (err, appointments) {
        if (!err) {
            return res.json(appointments);

        } else {
            res.statusCode = 500;

            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.get('/service/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var serviceId = req.params.id;
    var shopId = req.query.shop;

    var search = { 'shop': shopId }
    var services =  [serviceId]
    search.services = { "$in": services};

    Appointment.find( search, function (err, docs) {

        if (err) {
            res.statusCode = 500;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.get('/shop/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var shopId = req.params.id;

    var search = { 'shop': shopId }
    if(req.query.date) search.start = { "$regex": req.query.date, "$options": "i" };

    Appointment.find( search, function (err, docs) {

        if (err) {
            res.statusCode = 500;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.get('/pendingAppointmentsByEmployee/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var employee = req.params.id;

    var search = { 'employee': employee }
    var status =  ['Pendiente', 'Confirmado']
    search.status = { "$in": status};
    //if(req.query.date) search.start = { "$regex": req.query.date, "$options": "i" };

    Appointment.find( search, function (err, docs) {

        if (err) {
            res.statusCode = 500;
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            if(docs && docs.length){
                var appointments = []
                docs.forEach(function (appointment) {
                    var date = new Date(appointment.start)
                    var today = new Date()
                    today.setHours(0, 0, 0, 0)
                    if (date > today){
                     appointments.push(appointment)
                    }
                })
                return res.json(appointments.length);
            }
            else{
                return res.json(docs.length);
            }
        }
    });
});

router.get('/customer/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var customerId = req.params.id;

    Appointment.find({ 'customer': customerId }, function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Shop with id: %s Not Found', customerId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.get('/employee/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var employee = req.params.id;

    Appointment.find({ 'employee': employee }, function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Appointments with id: %s Not Found', employee);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.get('/count/employee/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var employee = req.params.id;

    var search = { 'employee': employee }
    var status =   ['Pendiente', 'Confirmado', 'Cancelado', 'Pagado','Eliminado','Ocupado']
    search.status = { "$in": status};

    Appointment.find(search, function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Appointments with id: %s Not Found', employee);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs.length);
        }
    });
});

router.post('/date/', passport.authenticate('bearer', { session: false }), function(req, res) {

    var date = req.body.date;
    var day =date.substring(0,2);
    var month = date.substring(3,5);
    var year = date.substring(6,10);
    var newDate = year+'-'+month+'-'+day;

    if(req.body.dresser){
        Appointment.find({
            'shop':req.body.shop,
            'employee':req.body.dresser,
            "start": {$regex : newDate}
        }, function (err, docs) {

            if (err) {
                res.statusCode = 500;
                log.error('Shop with id: %s Not Found', shopId);
                return res.json({
                    error: 'Not found',
                    fullError: err
                });
            } else {
                return res.json(docs);
            }
        });
    }else{
        Appointment.find({
            'shop':req.body.shop,
            "start": {$regex : newDate}
        }, function (err, docs) {

            if (err) {
                res.statusCode = 500;
                log.error('Shop with id: %s Not Found', shopId);
                return res.json({
                    error: 'Not found',
                    fullError: err
                });
            } else {
                return res.json(docs);
            }
        });
    }
});

router.post('/', passport.authenticate('bearer', { session: false }), function(req, res) {

    var connDetails = req.headers['x-real-ip'] + "-" + req.headers['user-agent'] + "-" + req.headers['accept-language'];
    
    var proposedPriceDate = null;

    if (req.body.proposedServicesPriceSentToClient) {
        proposedPriceDate = Date.now()
    }
    
    var appointment = new Appointment({
        start: req.body.start,
        anonymousCustomer: req.body.anonymousCustomer,
        end: req.body.end,
        shop: req.body.shop,
        employee: req.body.employee,
        customer: req.body.customer,
        services: req.body.services,
        status: req.body.status,
        createdBy: req.body.createdBy,
        source: req.body.source,
        observaciones: req.body.observaciones,
        proposedServicesPrice: req.body.proposedServicesPrice,
        proposedServicesPriceSentToClient: proposedPriceDate,
        technicalProductList: req.body.technicalProductList,
        createdDate: Date.now(),
        createdDetails: connDetails,
        checkboxFeedback: req.body.checkboxFeedback,
        rating: req.body.rating,
        reviewDetails: req.body.reviewDetails,
        type: req.body.type
    });

    appointment.save(function (err) {
        if (!err) {
            log.info("New appointment created for (%s) via %s.", appointment.start, appointment.source);
            
            if(appointment.status==='Confirmado' && !appointment.anonymousCustomer){
                //ver si tiene en true el presupuesto y agregarlo al mensaje
                var budget = req.body.proposedServicesPriceSentToClient ? appointment.proposedServicesPrice : null
                Notifications.dispatch(req.body.customer,appointment._id,'new',budget)
            }else if(appointment.status==='Pendiente' && !appointment.anonymousCustomer){
                //ver si tiene en true el presupuesto y agregarlo al mensaje
                var budget = req.body.proposedServicesPriceSentToClient ? appointment.proposedServicesPrice : null
                Notifications.dispatch(req.body.customer,appointment._id,'newPending',budget)
            }
            
            return res.json({
                status: 'OK',
                appointment: appointment
            });
        } else {
            if(err.name === 'ValidationError') {
                res.statusCode = 400;
                res.json({
                    error: 'Validation error',
                    fullError:err
                });
            } else {
                res.statusCode = 500;

                log.error('Internal error(%d): %s', res.statusCode, err.message);

                res.json({
                    error: 'Server error'
                });
            }
        }
    });
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    
    Appointment.findById(req.params.id, function (err, appointment) {

        if(!appointment) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK',
                appointment: appointment
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
   
});
router.options('/:id', cors());

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    var articleId = req.params.id;
    var connDetails = req.headers['x-real-ip'] + "-" + req.headers['user-agent'] + "-" + req.headers['accept-language'];
    

    Appointment.findById(articleId, function (err, appointment) {
        if(!appointment) {
            res.statusCode = 404;
            log.error('Appointment with id: %s Not Found', articleId);
            return res.json({
                error: 'Appointment not found'
            });
        }
        var cancelled=false;
        var originalStart = appointment.start;
        if(req.body.status==='Cancelado' && appointment.status!=='Cancelado')cancelled=true;
        var confirmed=false;
        if(req.body.status==='Confirmado' && appointment.status!=='Confirmado')confirmed=true;
        var dateChanged=false;
        if (req.body.start!==appointment.start) dateChanged=true;
        var pagado= false;
        if(req.body.status==='Pagado' && appointment.status!=='Pagado')pagado=true;
       
        
        appointment.start= req.body.start || appointment.start;
        appointment.end= req.body.end || appointment.end;
        appointment.shop= req.body.shop || appointment.shop;
        appointment.employee= req.body.employee || appointment.employee;
        appointment.services= req.body.services || appointment.services;
        appointment.images= req.body.images || appointment.images;
        appointment.status= req.body.status || appointment.status;
        appointment.source= req.body.source || appointment.source;
        appointment.recommended = req.body.recommended || appointment.recommended;
        appointment.substatus = req.body.substatus || appointment.substatus;
        appointment.totalPrice = req.body.totalPrice || appointment.totalPrice;
        appointment.observaciones = req.body.observaciones || appointment.observaciones;
        appointment.proposedServicesPrice = req.body.proposedServicesPrice || appointment.proposedServicesPrice;
        appointment.technicalProductList = req.body.technicalProductList || appointment.technicalProductList;
        appointment.checkboxFeedback = req.body.checkboxFeedback || appointment.checkboxFeedback;
        appointment.rating = req.body.rating || appointment.rating;
        appointment.reviewDetails = req.body.reviewDetails || appointment.reviewDetails;
        appointment.type = req.body.type || appointment.type;


        if (req.body.proposedServicesPriceSentToClient) {
            appointment.proposedServicesPriceSentToClient = Date.now();
        }
        if (typeof (req.body.anonymousCustomer) !== 'undefined') {
            log.info("Anoymous is %s", req.body.anonymousCustomer);
            appointment.anonymousCustomer = req.body.anonymousCustomer;
        }
        if (typeof (req.body.customer) !== 'undefined') {
            appointment.customer= req.body.customer;
        }
         //track changes
        appointment.lastUpdatedDate = Date.now();
        appointment.lastUpdatedBy = req.user;
        appointment.lastUpdatedDetails = connDetails;

        appointment.save(function (err) {
            if (!err) {
                if (!appointment.anonymousCustomer) {
                    if(cancelled){
                       Notifications.dispatch(req.body.customer,appointment._id,'cancel');
                    } 
                    else if (confirmed) {
                        var budget = req.body.proposedServicesPriceSentToClient ? appointment.proposedServicesPrice : null
                       Notifications.dispatch(req.body.customer,appointment._id,'confirm', budget)
                    } 
                    else if (dateChanged && !pagado) {
                        //ver si tiene en true el presupuesto y agregarlo al mensaje
                        var budget = req.body.proposedServicesPriceSentToClient ? appointment.proposedServicesPrice : null
                       Notifications.dispatch(req.body.customer,appointment._id,'mod', budget)
                    }
                    else if (pagado){
                        if (appointment.recommended && appointment.recommended.length > 0){
                            log.info("enviando recomendados");
                            Notifications.dispatch(req.body.customer,appointment._id,'recommended')
                        }
                        if(appointment.checkboxFeedback){
                            log.info("enviando feedback");
                            Notifications.feedback(appointment._id,appointment.shop,appointment.customer, req.body.domainLink);
                        }
                    }
                }
                
                return res.json({
                    status: 'OK',
                    appointment:appointment
                });
            } else {
                if(err.name === 'ValidationError') {
                    res.statusCode = 400;
                    return res.json({
                        error: 'Validation error',
                        fullError: err
                    });
                } else {
                    res.statusCode = 500;

                    return res.json({
                        error: 'Server error'
                    });
                }
                log.error('Internal error (%d): %s', res.statusCode, err.message);
            }
        });
    });
});

router.delete('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    Appointment.findByIdAndRemove(req.params.id, function (err, appointment) {

        if(!appointment) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK'
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.post('/image/', passport.authenticate('bearer', { session: false }), function (req, res) {
    var appointmentId = req.body.id;

    Appointment.findOne({'_id':appointmentId}, function (err, appointment) {
            if (err) {
                res.status(500).send(err);
            } else if(!appointment) {
                res.statusCode = 404;
                log.error('Appointment with id: %s Not Found', appointmentId);
                return res.json({
                    error: 'Appointment not found'
                });
            }else{
                var images = (typeof (appointment.images) == 'undefined'||!appointment.images) ? [] : appointment.images;
                images.push(req.body.photoUrl);
                appointment.images= images;

                appointment.save(function (err, appointment) {
                    if (!err) {
                        log.info("Appointment with id: %s modified", appointment.id);

                        return res.json({
                            status: 'OK',
                            appointment: appointment
                        });
                    } else {
                        if(err.name === 'ValidationError') {
                            res.statusCode = 400;
                            return res.json({
                                error: 'Validation error!',
                                fullError: err
                            });
                        } else {
                            res.statusCode = 500;
                            log.error('Internal error (%d): %s', res.statusCode, err.message);

                            return res.json({
                                error: 'Server error',
                                fullError: err
                            });
                        }
                    }
                });
            }
        }
    );
});



router.get('/technicalproduct/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    log.info("Searching stock in appointment...")
    var shopId = req.query.shop;
    var productId = req.params.id;
    var search = { 'shop': shopId }

    Appointment.find( search , function (err, appointments) {
        if (err) {
            res.statusCode = 500;
            log.error('Appointments with id: %s Not Found', productId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            var result = [];
            appointments.forEach(function (appointment) {
                var list = appointment.technicalProductList;
                if (list && list.length) {
                    list.map(function (technicalProduct) {
                        if(technicalProduct.product.toString() === productId.toString()){
                            var customerName = appointment.customer ? (appointment.customer.surname ? (appointment.customer.name + " " + appointment.customer.surname) : appointment.customer.name) : '';
                            var employeeName = appointment.employee ? (appointment.employee.surname ? (appointment.employee.name + " " + appointment.employee.surname) : appointment.employee.name) : '';
                            result.push({
                                _id: appointment.id,
                                date: appointment.start,
                                quantity: technicalProduct.quantity,
                                format: technicalProduct.format,
                                customer : customerName,
                                employee : employeeName
                            });
                        }
                    });
                }
            });
            return res.json(result);
        }
    });
});

router.get('/nextAppointmentDate/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    log.info("nextAppointmentDate...")

    Appointment.findById(req.params.id, async function (err, appointment) {
        if(!appointment) {
            res.statusCode = 404;
            return res.json({
                error: 'Not found'
            });
        }
        if (!err && appointment) {
            return res.json(await verifyAppointment.verifyNextAppointment(appointment, true));

        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.post('/verifyDate/', passport.authenticate('bearer', { session: false }), function(req, res) {

    log.info("verifyDate...")

    var employeeId = req.body.employee;

    Employee.findById(employeeId, async function (err, employee) {
        if (!err ) {
            var formDate = req.body.formDate;
            var startTime = new Date(req.body.start);
            var endTime = new Date(req.body.end);

            var formIniTime = new Date(formDate);
            formIniTime.setHours(startTime.getHours());
            formIniTime.setMinutes(startTime.getMinutes());

            var formEndTime = new Date(formDate);
            formEndTime.setHours(endTime.getHours());
            formEndTime.setMinutes(endTime.getMinutes());

            await verifyAppointment.getAppointmentsByEmployee(employee, async function(appointments) {
                var offset1 = Moment().utcOffset();
                var formIniTimeString = Moment.utc(formIniTime).utcOffset(offset1).format('YYYY-MM-DD');
                var list = appointments.filter(function (appointment) {
                    return appointment.start.includes(formIniTimeString)
                })

                var isCorrect = await verifyAppointment.isFreeDate(employee,formIniTime, list)
                // si shop y employee no esta de vacaciones ese dia y en su horario
                while(!isCorrect){
                    formIniTime.setDate(formIniTime.getDate() + 1);
                    formEndTime.setDate(formIniTime.getDate());
                    var offset = Moment().utcOffset();
                    var timeString = Moment.utc(formIniTime).utcOffset(offset).format('YYYY-MM-DD');
                    list = appointments.filter(function (appointment) {
                        return appointment.start.includes(timeString)
                    })
                    log.info("list1")
                    log.info(list.length)
                    isCorrect = await verifyAppointment.isFreeDate(employee,formIniTime,list)
                }

                return res.json({
                    status: 'OK',
                    formIniTime: formIniTime,
                    formEndTime: formEndTime,
                    isCorrect: isCorrect
                });
            });
        } else {
            log.error('Error getting Employee with id: %s ', employeeId);
            res.status(500).send(err);
        }
    });
});


module.exports = router;
