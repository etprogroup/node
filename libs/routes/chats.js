var express = require('express');
var passport = require('passport');
var router = express.Router();

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var Chat = require(libs + 'model/chat');
var Notifications = require(libs + 'modules/notifications');

router.post('/', passport.authenticate('bearer', { session: false }), function(req, res) {

    var customer = req.body.customer;
    var employee = req.body.employee;
    var appointment = req.body.appointment;
    var data = {
        'employee': employee,
        'customer': customer,
        'appointment': appointment
    };

    Chat.find(data, function (err, chats) {

        if(!err){
            allChats=[];
            for(message in chats){
                var newChat={
                    'employee': chats[message].employee._id,
                    'customer': chats[message].customer._id,
                    'appointment': chats[message].appointment._id,
                    'message': chats[message].message,
                    'sendby': chats[message].sendby,
                    'time': chats[message].time,
                }

                allChats.push(newChat)
            }

            return res.json({
                status: 'OK',
                data: data,
                chats: allChats,
            });

        }else{
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.post('/send/', passport.authenticate('bearer', { session: false }), function(req, res) {

    var customer = req.body.customer;
    var employee = req.body.employee;
    var appointment = req.body.appointment;

    var chat = new Chat({
        customer: customer,
        employee: employee,
        appointment: appointment,
        message: req.body.message,
        sendby: req.body.sendby,
        update: Date.now(),
        time: Date.now(),
    });

    chat.save(function (err) {
        if (!err) {
            log.info("New chat created");

            //ALL MESSAGE
            Chat.find({
                'employee': employee,
                'customer': customer,
                'appointment': appointment
            }, function (err, chats) {

                if(!err){
                    allChats=[];
                    for(message in chats){
                        var newChat={
                            'employee': chats[message].employee._id,
                            'customer': chats[message].customer._id,
                            'appointment': chats[message].appointment._id,
                            'message': chats[message].message,
                            'sendby': chats[message].sendby,
                            'time': chats[message].time,
                        }

                        allChats.push(newChat)
                    }

                    return res.json({
                        status: 'OK',
                        chat: chat,
                        chats: allChats,
                    });

                }else {
                    res.statusCode = 500;
                    log.error('Internal error(%d): %s',res.statusCode,err.message);

                    return res.json({
                        error: 'Server error'
                    });
                }
            });

            /*
            return res.json({
                status: 'OK',
                chat: chat,
                chats: allChats,
            });
            */

        } else {
            if(err.name === 'ValidationError') {
                res.statusCode = 400;
                res.json({
                    error: 'Validation error',
                    fullError:err
                });
            } else {
                res.statusCode = 500;

                log.error('Internal error(%d): %s', res.statusCode, err.message);

                res.json({
                    error: 'Server error'
                });
            }
        }
    });
});

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

    var messageID = req.params.id;
    Chat.findById(messageID, function (err, chat) {
        if(!chat) {
            res.statusCode = 404;
            log.error('chat with id: %s Not Found', messageID);
            return res.json({
                error: 'chat not found'
            });
        }

        chat.customer= req.body.customer || chat.customer;
        chat.employee= req.body.employee || chat.employee;
        chat.appointment= req.body.appointment || chat.appointment;
        chat.message= req.body.message || chat.message;
        chat.sendby= chat.sendby;
        chat.time= chat.time;
        chat.update= Date.now();

        if(chat.sendby === req.body.sendby){
            chat.save(function (err) {
                if (!err) {
                    log.info("chat updated");
                    if(chat.sendby === 'customer'){
                        Notifications.dispatch(chat.employee,chat._id,'mod')

                    }else if(chat.sendby === 'employee'){
                        Notifications.dispatch(chat.customer,chat._id,'mod')

                    }else{
                        Notifications.dispatch(chat.customer,chat._id,'mod')
                        Notifications.dispatch(chat.employee,chat._id,'mod')
                    }

                    return res.json({
                        status: 'OK',
                        chat:chat
                    });
                } else {
                    if(err.name === 'ValidationError') {
                        console.log(err);
                        res.statusCode = 400;
                        return res.json({
                            error: 'Validation error',
                            fullError: err
                        });
                    } else {
                        res.statusCode = 500;

                        return res.json({
                            error: 'Server error'
                        });
                }
                    log.error('Internal error (%d): %s', res.statusCode, err.message);
                }
            });

        }else{
            log.info("Message can't update by user");
            return res.json({
                error: 'Error in user',
            });
        }
    });
});
/**/

router.get('/appointment/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var appointment = req.params.id;

    Chat.find({ 'appointment': appointment }, function (err, chats) {
        if (err) {
            res.statusCode = 500;
            log.error('Chats with id: %s Not Found', appointment);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(chats);
        }
    });
});

router.delete('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    Chat.findByIdAndRemove(req.params.id, function (err, chat) {

        if(!chat) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK'
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

module.exports = router;
