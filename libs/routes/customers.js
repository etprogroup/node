var express = require('express');
var passport = require('passport');
var router = express.Router();
var cors = require('cors');

var libs = process.cwd() + '/libs/';

var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var Employee = require(libs + 'model/employee');
var Customer = require(libs + 'model/customer');
var Shop = require(libs + 'model/shop');
var Mailing = require(libs + 'modules/mailing');

var plivo = require('plivo');
var p = plivo.RestAPI({
    authId: 'MAZWFHY2Y2OWU4YJDING',
    authToken: 'N2RiOGI0OTI3OTllMWFmMWNkMWIyODY0NzE2ODIw'
});

function sendMessage(phoneNumber,shopPhone){


    
    var params = {
        'src': '+34 '+shopPhone,
        'dst' : '+34'+phoneNumber, // Receiver's phone Number with country code
        'text' : "Consigue un 10% de descuento en tu próxima reserva pidiéndola desde nuestra app. Descárgala en www.magnifique.me",
        'url' : "http://example.com/report/", // The URL to which with the status of the message is sent
        'method' : "GET" // The method used to call the url
    };

// Prints the complete response
    p.send_message(params, function (status, response) {
        console.log('Status: ', status);
        console.log('API Response:\n', response);
        console.log('Message UUID:\n', response['message_uuid']);
        console.log('Api ID:\n', response['api_id']);
    });

    log.info('Message sent to:'+phoneNumber)
}

//Mail
var nodemailer = require('nodemailer');
//var sendgrid = require("sendgrid")("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");

router.get('/', passport.authenticate('bearer', { session: false }), function(req, res) {

    Customer.find(function (err, customers) {
        if (!err) {
            return res.json(customers);
        } else {
            res.statusCode = 500;

            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error',
                fullError: err
            });
        }
    });
});

router.get('/listProfiles', passport.authenticate('bearer', { session: false }), function(req, res) {

    var search = {}

    search.hairType = {$exists:true } ;

    Customer.find(search, function (err, customers) {
        if (!err) {
            return res.json(customers);
        } else {
            res.statusCode = 500;

            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error',
                fullError: err
            });
        }
    });
});

router.get('/list', passport.authenticate('bearer', { session: false }), function(req, res) {

    Customer.find(function (err, customers) {
        if (!err) {
            var result = [];
            customers.map(function (v) {
                var customer = {
                    phoneNumber : v.phoneNumber ? v.phoneNumber : null,
                    name : v.name ? v.name : '',
                    surname : v.surname ? v.surname : '',
                    shop : v.shop ? v.shop : '',
                    email : v.email ? v.email : '',
                    platform : v.app.platform ? v.app.platform : ''
                }
                result.push(customer)
            })
            return res.json(result);
        } else {
            res.statusCode = 500;

            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error',
                fullError: err
            });
        }
    });
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    Customer.findById(req.params.id, function (err, customer) {

        if(!customer) {
            res.statusCode = 404;

            return res.json({
                error: 'Customer not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK',
                customer: customer
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.get('/phone/:phoneNumber', passport.authenticate('bearer', { session: false }), function(req, res) {

    var phoneNumber = req.params.phoneNumber;

    Customer.findOne({'phoneNumber': phoneNumber}, function (err, customer) {

        if(!customer) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {

            return res.json({
                status: 'OK',
                customer: customer
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.get('/shop/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var shopId = req.params.id;

    Customer.find({ 'shop': shopId }, function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.options('/:id', cors());

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    var customerId = req.params.id;
    var email = req.body.email === "" ? null : req.body.email
    var pwd = req.body.password
    
    var connDetails = req.headers['x-real-ip'] + "-" + req.headers['user-agent'] + "-" + req.headers['accept-language'];
    
    Customer.findById(customerId, function (err, customer) {
        if(!customer) {
            res.statusCode = 404;
            log.error('Shop with id: %s Not Found', customerId);
            return res.json({
                error: 'Not found'
            });
        }

        if(pwd){
            customer.password=req.body.password;
        }

        console.log(req.body);
        //TODO:Change customer parameters
        customer.shop = req.body.shop || customer.shop;
        customer.email = req.body.email || customer.email;
        customer.image = req.body.image || customer.image;
        customer.name = req.body.name || customer.name;
        customer.surname = req.body.surname || customer.surname;
        customer.phoneNumber = req.body.phoneNumber || customer.phoneNumber;
        customer.sex= req.body.sex || customer.sex;
        customer.birthday= req.body.birthday || customer.birthday;
        customer.language= req.body.language || customer.language;
        customer.nif= req.body.nif || customer.nif;
        customer.hairType= req.body.hairType || customer.hairType;
        customer.hairTexture= req.body.hairTexture || customer.hairTexture;
        customer.hairColor= req.body.hairColor || customer.hairColor;
        customer.hairState= req.body.hairState || customer.hairState;
        customer.nailType= req.body.nailType || customer.nailType;
        customer.nailPolish= req.body.nailPolish || customer.nailPolish;
        customer.skinType= req.body.skinType || customer.skinType;
        customer.eyelashType= req.body.eyelashType || customer.eyelashType;
        customer.eyebrowType= req.body.eyebrowType || customer.eyebrowType;
        customer.favouritesServices= req.body.favouritesServices || customer.favouritesServices;
        customer.favouritesProducts= req.body.favouritesProducts || customer.favouritesProducts;
        customer.shippingAddress= req.body.shippingAddress || customer.shippingAddress;
        customer.billingAddress= req.body.billingAddress || customer.billingAddress;
        customer.cardLastNumbers= req.body.cardLastNumbers || customer.cardLastNumbers;
        customer.FCMToken= req.body.FCMToken || customer.FCMToken;
        
        //track changes
        customer.lastUpdatedDate = Date.now();
        customer.lastUpdatedBy = req.user;
        customer.lastUpdatedDetails = connDetails;
            

        customer.save(function (err) {
            if (!err) {
                log.info("Shop %s: Customer (%s,%s,%s,%s) updated by %s.", customer.shop, 
                         customer.name, customer.surname, customer.phoneNumber, customer.email, req.user.name);

                return res.json({
                    status: 'OK',
                    customer: customer
                });
            } else {
                console.log(err);
                if(err.name === 'ValidationError') {
                    res.statusCode = 400;
                    return res.json({
                        error: 'Validation error',
                        fullError: err
                    });
                } else {
                    res.statusCode = 500;
                    log.error('Internal error (%d): %s', res.statusCode, err.message);

                    return res.json({
                        error: 'Server error'
                    });
                }
            }
        });
    });
});

router.post('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    var shopId = req.body.shop;
    
    var connDetails = req.headers['x-real-ip'] + "-" + req.headers['user-agent'] + "-" + req.headers['accept-language'];


    Shop.findById(shopId, function (err, shop) {
        if(!shop) {
            res.statusCode = 404;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found'
            });
        }

        var newCustomer = new Customer({
            shop: shopId,
            image: req.body.image,
            name: req.body.name,
            surname: req.body.surname,
            sex: req.body.sex,
            email: req.body.email,
            phoneNumber: req.body.phoneNumber,
            password: req.body.password,
            birthday: req.body.birthday,
            language: req.body.language,
            nif: req.body.nif,
            hairType: req.body.hairType,
            hairTexture: req.body.hairTexture,
            hairColor: req.body.hairColor,
            hairState: req.body.hairState,
            nailType: req.body.nailType,
            nailPolish: req.body.nailPolish,
            skinType: req.body.skinType,
            eyelashType: req.body.eyelashType,
            eyebrowType: req.body.eyebrowType,
            favouritesProducts: req.body.favouritesProducts,
            favouritesServices: req.body.favouritesServices,
            shippingAddress: req.body.shippingAddress,
            billingAddress: req.body.billingAddress,
            FCMToken: req.body.FCMToken,
            createdDate: Date.now(),
            createdBy: req.user,
            createdDetails: connDetails
            
        });

        newCustomer.save(function (err) {
            if (!err) {
                log.info("Shop %s: Customer (%s,%s,%s,%s) updated by %s.", newCustomer.shop, 
                         newCustomer.name, newCustomer.surname, newCustomer.phoneNumber, newCustomer.email, req.user.name);
       

                /*
                if(req.body.sendMessage){
                    var shopPhone = shop.phone || '1111111111'
                    sendMessage(req.body.phoneNumber,shopPhone)
                }
                */

                return res.json({
                    status: 'OK',
                    customer: newCustomer
                });
            } else {
                if(err.name === 'ValidationError') {
                    res.statusCode = 400;
                    return res.json({
                        error: 'Validation error!',
                        fullError: err,
                        customer: newCustomer
                    });
                } else {
                    res.statusCode = 500;
                    console.error('Internal error (%d): %s', res.statusCode, err.message);

                    return res.json({
                        error: 'Server error',
                        fullError: err,
                        customer: newCustomer
                    });
                }
            }
        });
    });
});

router.post('/new-customer/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    var customerId = req.params.id;
    var app={
        platform:req.body.platform,
        created:Date.now()
    };

    Customer.findByIdAndUpdate(customerId, {
        $set: {
            'app': app
        }
    }, { upsert: true }, function (err, customer) {

        if(!err){
            return res.json({
                message:'OK',
                update:customer.app
            })
        }else{
            return res.json({
                error:err
            })
        }

    })
});

router.post('/email/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    var customerId = req.params.id;
    var appointmentId = req.body.appointment;
    var shopId = req.body.shop;
    var file = req.body.pdf;

    Customer.findById(customerId, function (err, customer) {

        if(!customer) {
            res.statusCode = 404;
            return res.json({
                error: 'Customer not found'
            });
        }
        if (!err) {
            var email = req.body.email !== null && req.body.email !== '' ? req.body.email : customer.email;
            if(email){
                Mailing.sendInvoice(appointmentId,shopId,customer, file, email);

                return res.json({
                    status: 'OK',
                    customer:customer
                });
            }
            else
                return res.json({
                    status: 'The customer does not have email',
                    customer:customer
                });

        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.post('/emailToAdmin/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    var employeeId = req.params.id;
    var totalCustomers = req.body.totalCustomers;
    var savedCustomers = req.body.savedCustomers;

    Employee.findById(employeeId, function (err, employee) {

        if(!employee) {
            res.statusCode = 404;
            return res.json({
                error: 'Employee not found'
            });
        }
        if (!err) {
            if(employee.email){
                Mailing.sendSavedCustomersToAdmin(employee.email,totalCustomers, savedCustomers);

                return res.json({
                    status: 'OK',
                    employee:employee
                });
            }
            else
                return res.json({
                    status: 'The employee does not have email',
                    employee:employee
                });

        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.delete('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    Customer.findByIdAndRemove(req.params.id, function (err, customer) {

        if(!customer) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK'
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

module.exports = router;
