var express = require('express');
var passport = require('passport');
var router = express.Router();
var cors = require('cors');

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var mongoose = require('mongoose');
var Product = require(libs + 'model/product');
var Profile = require(libs + 'model/profile');
var Shop = require(libs + 'model/shop');

router.get('/', passport.authenticate('bearer', {session: false}), function (req, res) {

    var search = {}
    if(req.query.public){
        search.public = req.query.public ;
    }
    if(req.query.brand){
        search.brand = req.query.brand ;
    }
    if(req.query.general){
        search.shop = {$exists:false } ;
    }

    Product.find(search, function (err, products) {
        if (!err) {
            return res.json(products);
        } else {
            res.statusCode = 500;

            log.error('Internal error(%d): %s', res.statusCode, err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.get('/:id', passport.authenticate('bearer', {session: false}), function (req, res) {

    Product.findById(req.params.id, function (err, product) {

        if (!product) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK',
                product: product
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s', res.statusCode, err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.get('/shop/:id', passport.authenticate('bearer', {session: false}), function (req, res) {

    var shopId = req.params.id;
    var search = {'shop': shopId}
    if(req.query.brand){
        search.brand = req.query.brand ;
    }
    if(req.query.public){
        search.public = req.query.public ;
    }
    Product.find(search, function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.options('/:id', cors());

router.put('/:id', passport.authenticate('bearer', {session: false}), function (req, res) {
    var productId = req.params.id;

    Product.findById(productId, function (err, product) {
        if (!product) {
            res.statusCode = 404;
            log.error('Shop with id: %s Not Found', productId);
            return res.json({
                error: 'Not found'
            });
        }
//TODO:Change product parameters
        product.name = req.body.name || product.name
        product.barCode = req.body.barCode || product.barCode
        product.sku = req.body.sku || product.sku
        product.brand = req.body.brand || product.brand
        product.line = req.body.line || product.line
        product.category = req.body.category || product.category
        product.description = req.body.description || product.description
        product.large_description = req.body.large_description || product.large_description
        product.size = req.body.size || product.size
        product.price = req.body.price || product.price
        product.salePrice = req.body.salePrice || product.salePrice
        product.stock = req.body.stock || product.stock
        product.sex = req.body.sex || product.sex
        product.salonType = req.body.salonType || product.salonType
        product.format = req.body.format || product.format
        product.profile = req.body.profile || product.profile
        product.images = req.body.images || product.images
        product.videos = req.body.videos || product.videos
        product.stockList = req.body.stockList || product.stockList
        product.shop = req.body.shop || product.shop
        product.public = typeof req.body.public !== 'undefined' ? req.body.public : product.public
        product.minimum_stock = req.body.minimum_stock || product.minimum_stock
        product.purchase_price = req.body.purchase_price || product.purchase_price

        product.save(function (err) {
            if (!err) {
                log.info("Product with id: %s updated", product.id);
                return res.json({
                    status: 'OK',
                    product: product
                });
            } else {
                if (err.name === 'ValidationError') {
                    res.statusCode = 400;
                    return res.json({
                        error: 'Validation error',
                        fullError: err
                    });
                } else if (err.name === 'MongoError') {
                    res.statusCode = 400;
                    return res.json({
                        error: 'Validation error',
                        fullError: err
                    });
                } else {
                    res.statusCode = 500;
                    log.error('Internal error (%d): %s', res.statusCode, err.message);

                    return res.json({
                        error: err
                    });
                }
            }
        });
    });
});

//NEW SERVICE
router.post('/', passport.authenticate('bearer', {session: false}), function (req, res) {
    var productId = req.params.id;

    var newProduct = new Product({

        name: req.body.name,
        barCode: req.body.barCode,
        sku: req.body.sku,
        brand: req.body.brand,
        line: req.body.line,
        category: req.body.category,
        description: req.body.description,
        large_description: req.body.large_description,
        size: req.body.size,
        price: req.body.price,
        stock: req.body.stock,
        salePrice: req.body.salePrice,
        sex: req.body.sex,
        salonType: req.body.salonType,
        format: req.body.format,
        profile: req.body.profile,
        images: req.body.images,
        stockList: req.body.stockList,
        videos: req.body.videos,
        public: req.body.public,
        shop: req.body.shop,
        minimum_stock: req.body.minimum_stock,
        purchase_price: req.body.purchase_price
    });

    newProduct.save(function (err) {
        if (!err) {
            log.info("Product with id: %s inserted", newProduct.id);

            return res.json({
                status: 'OK',
                product: newProduct
            });
        } else {
            if (err.name === 'ValidationError') {
                res.statusCode = 400;
                return res.json({
                    error: 'Validation error!',
                    fullError: err
                });
            } else {
                res.statusCode = 500;
                log.error('Internal error (%d): %s', res.statusCode, err.message);

                return res.json({
                    error: 'Server error',
                    fullError: err
                });
            }
        }
    });
});

router.get('/brand/:id', passport.authenticate('bearer', {session: false}), function (req, res) {

    var brandId = req.params.id;

    Product.find({'brand': brandId}, function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Brand with id: %s Not Found', brandId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.get('/brands/:id', passport.authenticate('bearer', {session: false}), function (req, res) {
    var search = {}

    if (req.params.id) {
        var list = req.params.id;
        var brands = list.split(",");
        var brandsIds = [];
        brands.forEach(function (brand) {
            brandsIds.push(new mongoose.Types.ObjectId(brand));
        });
        search.brand = {"$in": brandsIds};
    }

    Product.find(search, function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Internal error(%d): %s', res.statusCode, err.message);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.delete('/:id', passport.authenticate('bearer', {session: false}), function (req, res) {

    Product.findByIdAndRemove(req.params.id, function (err, product) {

        if (!product) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK'
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s', res.statusCode, err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

module.exports = router;
