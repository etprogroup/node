var express = require('express');
var passport = require('passport');
var router = express.Router();

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var AppointmentTemp = require(libs + 'model/appointmentTemp');
var Appointment = require(libs + 'model/appointment');
var Customer = require(libs + 'model/customer');
var Service = require(libs + 'model/service');
var Shop = require(libs + 'model/shop');
var Employee = require(libs + 'model/employee');
var Notifications = require(libs + 'modules/notifications');
var verifyAppointment = require(libs + 'modules/verifyAppointment');
var Moment = require('moment');
var request = require('request');
var async = require('async');

async function sendResponseFacebook(facebookId, response, PAGE_ACCESS_TOKEN, callback){
    var url = "https://graph.facebook.com/v2.6/me/messages";
    //var url = "https://graph.facebook.com/v2.6/me/messages?access_token="+PAGE_ACCESS_TOKEN;
    var body = {
        "access_token": PAGE_ACCESS_TOKEN,
        "recipient": {"id": facebookId},
        "message": response
    }

    var options = {
        "uri": url,
        "qs": { "access_token": PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": body
    }

    await request(options, async (err, res, body) => {
        if (!err) {
            //console.log('message sent!');
            await callback({response:body,request:response}, true);
            return true;
        } else {
            //console.error("Unable to send message:" + err);
            await callback(body, false);
            return false;
        }
    });
}

async function getUserFacebook(facebookId, PAGE_ACCESS_TOKEN){
    //var url = 'https://graph.facebook.com/v2.6/'+facebookId+'?fields=name,first_name,last_name,email,photos,picture,profile_pic,locale,timezone,gender,link&access_token='+PAGE_ACCESS_TOKEN;
    var url = 'https://graph.facebook.com/v2.6/'+facebookId;
    var result = null;

    var params = {
        fields:'name,first_name,last_name,email,photos,picture,profile_pic,locale,timezone,gender,link',
        access_token: PAGE_ACCESS_TOKEN,
    };

    var options = {
        "uri": url,
        "qs": params,
        "method": "GET",
        "headers": { 'Content-Type': 'application/json' },
    }


    return new Promise(function(resolve, reject) {
        // Do async job
        request.get(options, function(err, resp, body) {
            if (err) {
                reject(err);
            } else {
                resolve(JSON.parse(body));
            }
        })
    });
    /*
    request(options, (err, res, body) => {
        if (!err) {
            //console.log('message sent!');
            return JSON.parse(body);

        } else {
            //console.error("Unable to send message:" + err);
            return {
                first_name : "name",
                last_name : "last_name",
                gender : "male",
                profile_pic : "profile_pic",
                options:options,
                params:params,
                url:url,
            };
        }
    });*/
}

async function createFacebookPostBack(data){
    var postBack = {
        "attachment": {
            "type":  "template",
            "payload": {
                "template_type":  "generic",
                //"elements": [{"title": data.title, "subtitle": data.subtitle, "image_url": data.image, "buttons": data.buttons}]
                "elements": data
            }
        }
    }
    return postBack;
}

async function createPostBackData(message,buttons){
    var dataNew=[];
    var count = 0;
    var grupView = 0;
    var grupButtons=[];
    var groupMessage=message;

    buttons.map((button)=>{
        grupButtons.push(button);
        count = count+1;

        if(grupView>0){
            groupMessage = 'Más';
        }

        if(count==3){
            var element={
                "title": groupMessage,
                "subtitle": "",
                "image_url": "",
                "buttons": grupButtons
            }


            if(grupButtons.length>0){
                dataNew.push(element);
            }

            grupView = grupView+1;
            grupButtons=[];
            count=0;
        }
    });

    if(count<3){
        var element={
            "title": groupMessage,
            "subtitle": "",
            "image_url": "",
            "buttons": grupButtons
        }

        if(grupButtons.length>0){
            dataNew.push(element);
        }
    }

    return dataNew;
}

async function createFacebookMessage(text){
    var message = {"text": text,}
    return message;
}


router.post('/', passport.authenticate('bearer', { session: false }), function(req, res) {
    return res.json({
        err: 'Error Not found Platform'
    });
});

router.post('/facebook/', passport.authenticate('bearer', { session: false }), function(req, res) {
    var customer = req.body.customer;
    return res.json({
        err: 'Error Not found Shop'
    });
});

router.get('/facebook/allShop/', function(req, res){
    var result = "";

    //SHOPS
    Shop.find( function (err, shops) {
        if (!err) {
            if(!shops){
                result = 'Shops no found';

            }else{
                shops.map((shop)=>{
                    result += "<div>ID: "+shop._id+" Name: "+shop.settings.name+"</div>";
                });
            }

        }else{
            result = 'Server error';
        }

        res.set('Content-Type', 'text/html');
        res.send(new Buffer(result));
        return res;
    });
});

router.get('/facebook/:shop/:token', async function(req, res){

    var shopId = req.params.shop;
    var PAGE_ACCESS_TOKEN = req.params.token;
    let hub_mode = req.query['hub.mode']; //req.query.hub_mode;
    let hub_verify_token = req.query['hub.verify_token']; //req.query.hub_verify_token;
    let hub_challenge = req.query['hub.challenge'];//req.query.hub_challenge;
    var VERIFY_TOKEN = "FaceChatbots";
    var result = "";

    if(hub_verify_token == VERIFY_TOKEN){
        result = hub_challenge;

    }else{
        result = "error";
    }

    //SHOP
    await Shop.findOne({'_id':shopId}, async function (err, shop) {
        if (!err) {
            if(!shop){
                result = 'Shop no found';
            }

            return;

        }else{
            result = 'Server error';
            return;
        }
    });

    res.set('Content-Type', 'text/html');
    res.send(new Buffer(result));
    return res;
});

router.post('/facebook/:shop/:token', async function(req, res){

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var body = req.body;
    var VERIFY_USER = null;
    var VERIFY_USERDetail = null;
    var VERIFY_APPOINTMENT = null;
    var VERIFY_SHOP = null;
    var VERIFY_SHOPService = null;
    var VERIFY_SHOPProducts = null;
    var VERIFY_SHOPEmployee = null;
    var VERIFY_Employee = [];
    var VERIFY_TOKEN = "FaceChatbots";
    var PAGE_ACCESS_TOKEN = req.params.token;
    var shopId = req.params.shop;
    var result = false;
    var message = 'Error';
    var response = createFacebookMessage(message);

    var hub_mode = req.params.hub_mode;
    var hub_verify_token = req.params.hub_verify_token;
    var hub_challenge = req.params.hub_challenge;

    function facebookResponseCallback(body, result){
        return res.json({
            body: body,
            result: result
        });
    }

    if(hub_verify_token == VERIFY_TOKEN){
        res.set('Content-Type', 'text/html');
        res.send(new Buffer(hub_challenge));
        return res;

    }else{

        var object = body.object;
        var facebookId = body['entry[0][messaging][0][sender][id]'];


        //CUSTOMER
        await Customer.findOne({'facebookId':facebookId}, async function (err, user) {
            if (!err) {
                VERIFY_USER = user;
                return;

            }else{
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });


        //SHOP
        await Shop.findOne({'_id':shopId}, async function (err, shop) {
            if (!err) {

                if(!shop){
                    message = 'Shop no found';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    response = createFacebookMessage(message);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                    return;

                }else{
                    VERIFY_SHOP = shop;
                    VERIFY_SHOPService = shop.services;
                    VERIFY_SHOPProducts = shop.products;
                    return;
                }



            }else{
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });

        //EMPLOYEE
        await Employee.find({'shop':shopId}, async function (err, employees) {
            if (!err) {

                if(!employees){
                    message = 'Employee no found';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    response = createFacebookMessage(message);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                    return;

                }else{
                    VERIFY_Employee = employees;
                    return;

                }


            }else{
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });

        //APPOINTMENT TEMP
        await AppointmentTemp.findOne({
            'reference':facebookId,
            'source':'facebook',
            'status':'pending'}, async function (err, appointmentTemp) {
            if (!err) {
                VERIFY_APPOINTMENT = appointmentTemp;
                return;

            }else{
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });

        //GET USER DETAIL
        VERIFY_USERDetail = await getUserFacebook(facebookId, PAGE_ACCESS_TOKEN);
        var VERIFY_USERDetailName = VERIFY_USERDetail.first_name;
        var VERIFY_USERDetailGender = VERIFY_USERDetail.gender;

        var gender = 'Mujer';
        if(VERIFY_USERDetailGender=='male'){
            gender = 'Hombre';
        }

        var data = "";
        var text = "";

        if(body['entry[0][messaging][0][message][text]']){
            data = body['entry[0][messaging][0][message][text]'];

        }else if(body['entry[0][messaging][0][message][attachments][0][type]']){
            var attachmentsType= body['entry[0][messaging][0][message][attachments][0][type]'];
            var attachmentsURL= body['entry[0][messaging][0][message][attachments][0][payload][url]'];

            data = 'text';
            text = 'Has enviado '+attachmentsType+':'+attachmentsURL;

        }else if(body['entry[0][messaging][0][message][postback][payload]']){
            data = body['entry[0][messaging][0][message][postback][payload]'];
        }

        //CREATE CHAT
        message = "Hola "+VERIFY_USERDetailName+" En que puedo ayudarte:";
        log.info('CHATBOT FACEBOOK message: '+message);
        var image = "https://www.magnifique.me/img/splash.png";
        var postBackData = await createPostBackData(message,[
            {"type": "postback", "title": "Servicio", "payload": "initservice"},
            {"type": "postback", "title": "Producto", "payload": "initproduct"},
            {"type": "postback", "title": "Conocernos", "payload": "about"},
            {"type": "postback", "title": "Horario", "payload": "inithour"}]);
        var response = await createFacebookPostBack(postBackData);

        data = data.trim();
        log.info('CHATBOT FACEBOOK data: '+data);

        if(data == 'initservice' || data == 'nservcustomer'){
            if(!VERIFY_USER && data == 'initservice'){

                message = 'Deseas un servicio mas personalizado';
                log.info('CHATBOT FACEBOOK message: '+message);
                postBackData = await createPostBackData(message,[
                    {"type": "postback", "title": "SI", "payload": "yservcustomer"},
                    {"type": "postback", "title": "NO", "payload": "nservcustomer"}]);
                //response = await createFacebookMessage(message);
                response = await createFacebookPostBack(postBackData);
                result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

            }else{

                if(VERIFY_SHOPService){
                    var services = [];
                    VERIFY_SHOPService.map((service)=>{
                        if(service.sex==gender){
                            services.push({"type": "postback", "title": service.name, "payload": 'listService_'+service._id});
                        }
                    });

                    message = 'Selecciona un servicio';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    postBackData = await createPostBackData(message,services);
                    response = await createFacebookPostBack(postBackData);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

                }else{
                    message = 'No hay servicios';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    response = await createFacebookMessage(message);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                }

            }

        }else if(data == 'yservcustomer'){
            message = 'Perfecto, escribe tu correo electrónico.';
            log.info('CHATBOT FACEBOOK message: '+message);
            response = await createFacebookMessage(message);
            result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);


        }else if(data.split(" ").length == 1 && data.indexOf("@") > -1 && data.indexOf(".",data.indexOf("@")) > -1){
            //EMAIL name,first_name,last_name,email,photos,picture,profile_pic,locale,timezone,gender
            var email = data;

            await Customer.findOne({'email':email}, async function (err, user) {
                if (!err) {

                    if(user){
                        VERIFY_USER = user;
                        VERIFY_USER.email = user.email ? user.email : email;

                    }else{
                        VERIFY_USER = new Customer();
                        VERIFY_USER.email = email;
                        VERIFY_USER.shop = shopId;

                    }

                }else {
                    message = 'Algo esta mal con la busqueda de usuario. Vamos a intentarlo nuevamente';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    response = await createFacebookMessage(message);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                }
            });

            VERIFY_USER.facebookId = facebookId;
            VERIFY_USER.facebook = JSON.stringify(VERIFY_USERDetail);
            VERIFY_USER.name = VERIFY_USERDetailName;
            VERIFY_USER.surname = VERIFY_USERDetail.last_name;
            VERIFY_USER.image = VERIFY_USERDetail.profile_pic;
            VERIFY_USER.sex = gender;

            await VERIFY_USER.save(async function (err) {
                if (!err) {
                    message = 'Hemos creado un usario con tu email. Empecemos de nuevo.';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    postBackData = await createPostBackData(message,[
                        {"type": "postback", "title": "Servicio", "payload": "initservice"},
                        {"type": "postback", "title": "Producto", "payload": "initproduct"},
                        {"type": "postback", "title": "Conocernos", "payload": "about"},
                        {"type": "postback", "title": "Horario", "payload": "inithour"}]);
                    response = await createFacebookPostBack(postBackData);
                    //response = await createFacebookMessage(message);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

                } else {
                    message = 'Algo esta mal gardando el usuario. Vamos a intentarlo nuevamente';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    response = await createFacebookMessage(message);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                }
            });


        }else if(data.indexOf("listService_") > -1){
            var service = data.replace("listService_","");
            var serviceList = [];
            serviceList.push(service)

            var customer = 0;
            if(VERIFY_USER){
                customer = VERIFY_USER._id;
            }

            if(!VERIFY_APPOINTMENT){
                VERIFY_APPOINTMENT = new AppointmentTemp();
                VERIFY_APPOINTMENT.reference = facebookId;
                VERIFY_APPOINTMENT.source = 'facebook';
            }

            VERIFY_APPOINTMENT.shop = shopId;
            VERIFY_APPOINTMENT.customer = customer;
            VERIFY_APPOINTMENT.services = serviceList;

            await VERIFY_APPOINTMENT.save(async function (err) {
                if (!err) {

                    if(VERIFY_Employee){
                        var employees = [];
                        VERIFY_Employee.map((employee)=>{
                            employees.push({"type": "postback", "title": employee.name, "payload": 'listEmployee_'+employee._id});
                        });

                        message = 'Selecciona un Estlista';
                        log.info('CHATBOT FACEBOOK message: '+message);
                        postBackData = await createPostBackData(message,employees);
                        response = await createFacebookPostBack(postBackData);
                        result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

                    }else{
                        message = 'No hay Empleados en este salon';
                        log.info('CHATBOT FACEBOOK message: '+message);
                        response = await createFacebookMessage(message);
                        result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                    }

                } else {
                    message = 'Algo esta mal gardando el servicio. Vamos a intentarlo nuevamente';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    response = await createFacebookMessage(message);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                }
            });


        }else if(data.indexOf("listEmployee_") > -1){
            var employee = data.replace("listEmployee_","");

            if(VERIFY_APPOINTMENT){
                VERIFY_APPOINTMENT.employee = employee;
                await VERIFY_APPOINTMENT.save(async function (err) {

                    if (!err) {

                        var day=null;
                        var days = [];
                        VERIFY_APPOINTMENT.start = new Date();

                        for(var i=0; i<9; i++){
                            var day = await verifyAppointment.verifyNextAppointment(VERIFY_APPOINTMENT, false);
                            var dayStart = day.formIniTime
                            var title = Moment(dayStart).format( 'DD-MM-YYYY H:m');//"dia: "+
                            days.push({"type": "postback", "title": title, "payload": 'listOptionDate_'+dayStart});

                            var newDate = new Date(dayStart);
                            newDate.setDate(newDate.getDate() + 1);
                            VERIFY_APPOINTMENT.start = newDate;
                            //VERIFY_APPOINTMENT.start = Moment(dayStart.add(1, 'days'); // day.formEndTime;
                        }

                        message = 'Guardamos el empleado. Ahora escoje un dia';
                        log.info('CHATBOT FACEBOOK message: '+message);
                        postBackData = await createPostBackData(message,days);
                        //response = await createFacebookMessage(message);
                        response = await createFacebookPostBack(postBackData);
                        result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

                    } else {
                        message = 'Algo esta mal gardando el empleado. Vamos a intentarlo nuevamente';
                        log.info('CHATBOT FACEBOOK message: '+message);
                        response = await createFacebookMessage(message);
                        result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                        return res.json({
                            err:err,
                        });
                    }
                });

            }else{
                message = 'Algo ha estado mal. No hay un reserva en proceso. Empecemos de nuevo';
                log.info('CHATBOT FACEBOOK message: '+message);
                postBackData = await createPostBackData(message,[
                    {"type": "postback", "title": "Servicio", "payload": "nservcustomer"},
                    {"type": "postback", "title": "Producto", "payload": "initproduct"},
                    {"type": "postback", "title": "Conocernos", "payload": "about"},
                    {"type": "postback", "title": "Horario", "payload": "inithour"}]);

                response = await createFacebookPostBack(postBackData);
                result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
            }


            //APPOINTMENT DAY
        }else if(data.indexOf("listOptionDate_") > -1){
            var day = data.replace("listOptionDate_",""); // new Date()

            if(VERIFY_APPOINTMENT){

                //services
                var services = VERIFY_APPOINTMENT.services;
                var servicePrice = 0;
                var serviceDuration = 0;
                var serviceName = "";
                var group = [];

                if(services.length>0) {
                    services.map((v)=> {
                        VERIFY_SHOP.services.map((w)=> {
                            if(v.equals(w._id)){
                                group.push({type:"service",v:v,w:w._id});
                                servicePrice += w.price;
                                serviceDuration += w.duration;
                                serviceName += w.name+" - ";
                            }
                        })
                    })
                }

                //employee
                var employee = VERIFY_APPOINTMENT.employee;
                var employeeName = "";

                VERIFY_Employee.map((w)=> {
                    if(employee.equals(w._id)){
                        group.push({type:"employee",v:employee,w:w._id});
                        employeeName = w.name;
                    }
                })

                var start = new Date(day);
                var end = new Date(start);
                var duration = serviceDuration*60*1000;
                var startFormat = Moment(start).format( 'DD-MM-YYYY H:m');

                end.setTime(end.getTime()+duration);
                VERIFY_APPOINTMENT.start = start.toISOString();
                VERIFY_APPOINTMENT.end = end.toISOString();

                await VERIFY_APPOINTMENT.save(async function (err) {

                    if (!err) {
                        var options=[];
                        options.push({"type": "postback", "title": "Confirmar", "payload": 'appointmenStatus_confirm'});
                        options.push({"type": "postback", "title": "Cancelar", "payload": 'appointmenStatus_cancel'});
                        options.push({"type": "postback", "title": "Revisar", "payload": 'appointmenStatus_check'});

                        message = 'Dia guardado. Tu cita: '+startFormat+", para:"+serviceName+", con:"+employeeName+", precio:"+servicePrice+"Euros";
                        log.info('CHATBOT FACEBOOK message: '+message);
                        postBackData = await createPostBackData(message,options);
                        response = await createFacebookPostBack(postBackData);
                        result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                        return res.json({
                            VERIFY_APPOINTMENT: VERIFY_APPOINTMENT,
                            formatStart:  start,
                            formatEnd:  end,
                            start:  Moment(start).format( 'DD-MM-YYYY H:m'),
                            end:  Moment(end).format( 'DD-MM-YYYY H:m'),
                        });

                    } else {
                        message = 'Ha ocurrido un error guardando el dia de la cita. Vamos a intentarlo nuevamente';
                        log.info('CHATBOT FACEBOOK message: '+message);
                        response = await createFacebookMessage(message);
                        result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                        return res.json({
                            err:err,
                        });
                    }
                });

            }else{
                message = 'Algo ha estado mal. No hay un reserva en proceso. Empecemos de nuevo';
                log.info('CHATBOT FACEBOOK message: '+message);
                postBackData = await createPostBackData(message,[
                    {"type": "postback", "title": "Servicio", "payload": "nservcustomer"},
                    {"type": "postback", "title": "Producto", "payload": "initproduct"},
                    {"type": "postback", "title": "Conocernos", "payload": "about"},
                    {"type": "postback", "title": "Horario", "payload": "inithour"}]);

                response = await createFacebookPostBack(postBackData);
                result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
            }


            //APPOINTMENT STATUS
        }else if(data.indexOf("appointmenStatus_") > -1){
            var option = data.replace("appointmenStatus_","");

            if(VERIFY_APPOINTMENT){
                var save = false;
                var customer = VERIFY_APPOINTMENT.customer;
                VERIFY_APPOINTMENT.status = option;


                if(option == 'confirm'){
                    var newAppointment = new Appointment();
                    newAppointment.customer = customer;
                    newAppointment.shop = VERIFY_APPOINTMENT.shop;
                    newAppointment.services = VERIFY_APPOINTMENT.services;
                    newAppointment.employee = VERIFY_APPOINTMENT.employee;
                    newAppointment.start = VERIFY_APPOINTMENT.start;
                    newAppointment.end = VERIFY_APPOINTMENT.end;

                    if(customer==0){
                        newAppointment.anonymousCustomer=true;
                        newAppointment.customer = null;
                    }

                    await newAppointment.save(async function (err) {
                        if (!err) {
                            save = true;

                        }else{
                            message = 'Ha ocurrido un error guardando la reserva real. Vamos a intentarlo nuevamente';
                            log.info('CHATBOT FACEBOOK message: '+message);
                            response = await createFacebookMessage(message);
                            result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                            return res.json({
                                VERIFY_APPOINTMENT: VERIFY_APPOINTMENT,
                                newAppointment: newAppointment,
                                err: err,
                            });

                        }
                    });

                }else if(option == 'cancel') {
                    message = 'Reserva cancelada. Que mas deseas hacer';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    postBackData = await createPostBackData(message,[
                        {"type": "postback", "title": "Servicio", "payload": "nservcustomer"},
                        {"type": "postback", "title": "Producto", "payload": "initproduct"},
                        {"type": "postback", "title": "Conocernos", "payload": "about"},
                        {"type": "postback", "title": "Horario", "payload": "inithour"}]);
                    postBackData = await createPostBackData(message,options);
                    response = await createFacebookPostBack(postBackData);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

                }else{
                    message = 'Empecemos de nuevo';
                    log.info('CHATBOT FACEBOOK message: '+message);
                    postBackData = await createPostBackData(message,[
                        {"type": "postback", "title": "Servicio", "payload": "nservcustomer"},
                        {"type": "postback", "title": "Producto", "payload": "initproduct"},
                        {"type": "postback", "title": "Conocernos", "payload": "about"},
                        {"type": "postback", "title": "Horario", "payload": "inithour"}]);
                    postBackData = await createPostBackData(message,options);
                    response = await createFacebookPostBack(postBackData);
                    result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

                }

                if(save){
                    await VERIFY_APPOINTMENT.save(async function (err) {
                        if (!err) {
                            message = 'Tu cita ha sido confirmada: ';
                            log.info('CHATBOT FACEBOOK message: '+message);
                            response = await createFacebookMessage(message);
                            result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                            return res.json({
                                VERIFY_APPOINTMENT: VERIFY_APPOINTMENT,
                                newAppointment: newAppointment,
                            });

                        } else {
                            message = 'Ha ocurrido un error guardando la reserva. Vamos a intentarlo nuevamente';
                            log.info('CHATBOT FACEBOOK message: '+message);
                            response = await createFacebookMessage(message);
                            result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
                            return res.json({
                                err:err,
                            });
                        }
                    });
                }

            }else{
                message = 'Algo ha estado mal. No hay un reserva en proceso. Empecemos de nuevo';
                log.info('CHATBOT FACEBOOK message: '+message);
                postBackData = await createPostBackData(message,[
                    {"type": "postback", "title": "Servicio", "payload": "nservcustomer"},
                    {"type": "postback", "title": "Producto", "payload": "initproduct"},
                    {"type": "postback", "title": "Conocernos", "payload": "about"},
                    {"type": "postback", "title": "Horario", "payload": "inithour"}]);

                response = await createFacebookPostBack(postBackData);
                result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
            }


        }else if(data == 'about'){
            message = 'Que deseas ver';
            log.info('CHATBOT FACEBOOK message: '+message);
            var postBackData = await createPostBackData(message,[
                {"type": "postback", "title": "Servicios", "payload": "allservice"},
                {"type": "postback", "title": "Empelados", "payload": "allemployee"}]);

            response = await createFacebookPostBack(postBackData);
            result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);


        }else if(data == 'allservice'){
            if(VERIFY_SHOPService){
                var services = [];
                VERIFY_SHOPService.map((service)=>{
                    if(service.sex==gender){
                        services.push({"type": "postback", "title": service.name, "payload": 'noaction'});
                    }
                });

                message = 'Estos son nuestros servicios';
                log.info('CHATBOT FACEBOOK message: '+message);
                postBackData = await createPostBackData(message,services);
                response = await createFacebookPostBack(postBackData);
                result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

            }else{
                message = 'No hay servicios';
                log.info('CHATBOT FACEBOOK message: '+message);
                response = await createFacebookMessage(message);
                result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
            }


        }else if(data == 'allemployee'){
            if(VERIFY_Employee){
                var employees = [];
                VERIFY_Employee.map((employee)=>{
                    employees.push({"type": "postback", "title": employee.name, "payload": 'noaction'});
                });

                message = 'Estos son nuestros estlistas';
                log.info('CHATBOT FACEBOOK message: '+message);
                postBackData = await createPostBackData(message,employees);
                response = await createFacebookPostBack(postBackData);
                result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

            }else{
                message = 'No hay Empleados en este salon';
                log.info('CHATBOT FACEBOOK message: '+message);
                response = await createFacebookMessage(message);
                result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);
            }


        }else if(data == 'noaction'){
            log.info('CHATBOT FACEBOOK message: '+message);
            result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);


        }else if(data == 'inithour'){
            var opening = VERIFY_SHOP.opening;
            var schedules = [];

            return res.json({
                opening: opening,
                schedules:schedules,
            });

            opening.map((day)=>{
                //var schedulesDay = index+": "+day.morning.start+" a "+evening.morning.end;
                //schedules.push({"type": "postback", "title": schedulesDay, "payload": 'noaction'});
            });

            message = 'Estos son nuestros Horarios';
            log.info('CHATBOT FACEBOOK message: '+message);
            postBackData = await createPostBackData(message,schedules);
            response = await createFacebookPostBack(postBackData);
            result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);


        }else if(data.indexOf("allAppointment_") > -1){
            var employee = data.replace("allAppointment_","");

            await Appointment.find({'employee':employee}, async function (err, appointments) {
                if (!err) {
                    return res.json({
                        appointments: appointments,
                    });
                    return;

                }else{
                    res.statusCode = 500;
                    log.error('Internal error(%d): %s',res.statusCode,err.message);

                    return res.json({
                        error: 'Server error',
                        err: err,
                    });
                }
            });


        }else if(data=="allUser"){
            await Customer.find({'facebookId':facebookId}, async function (err, users) {
                if (!err) {
                    return res.json({
                        users: users,
                    });
                    return;

                }else{
                    res.statusCode = 500;
                    log.error('Internal error(%d): %s',res.statusCode,err.message);

                    return res.json({
                        error: 'Server error',
                        err: err,
                    });
                }
            });


        }else{

            /*//TEST
            facebookResponseCallback({
                data: data,
                response: response,
                facebookId: facebookId,
                PAGE_ACCESS_TOKEN: PAGE_ACCESS_TOKEN,
            },true);
        */
            result = await sendResponseFacebook(facebookId,response,PAGE_ACCESS_TOKEN,facebookResponseCallback);

        }
    }
});

module.exports = router;


/*//GUIA
		Hola 'NAME' En que puedo ayudarte opciones POSTBACK(servicio=initservice,producto=initproduct,conocernos=about,horario=inithour)

-		initservice = if(!user){Deseas un servicio mas personalizado. POSTBACK (SI=yservcustomer, NO=nservcustomer)}
-		initservice = if(user){ Selecciona un servicio. POSTBACK list_service=listService_%service% }

		initproduct = Estos son nuestros productos POSTBACK (list_products=listProducts_%product%)

-		listService_%service% = if(user){ POSTBACK list_employee=listEmployee_%employee% }
-		listService_%service% = if(!user){ Para iniciar una reserva Necesitare tu email. %email% }

-		listEmployee_%employee% =  if(user){ POSTBACK list_optionDate=listOptionDate_%time% }
-		listEmployee_%employee% = if(!user){ Para iniciar una reserva Necesitare tu email. %email% }

		listOptionDate_%time% =   Confirma la reserva (DETAIL) POSTBACK (SI=yconfirm, NO=nconfirm)

		yconfirm = (new appointment) LISTO se te ha creado una cita. se Envio un email con la confirmacion.
		nconfirm = deseas actualizar el appointment POSTBACK (SI=initservice, NO=ncustom)

-		yservcustomer = Necesitare tu email. %email%
-		nservcustomer = Perfecto. En que puedo ayudarte. POSTBACK(conocernos=about,servicio=initservice,producto=initproduct,horario=inithour)

-		about = Que deseas ver POSTBACK (Ver servicios=allservice, Ver Empleados=allemployee)}
-		allservice = POSTBACK list_service=listViewService_%service%
-		allemployee = POSTBACK list_service=listViewEmployee_%employee%

		listViewService_%service% = Quieres iniciar una reserva POSTBACK (SI=listService_%service%, NO=ncustom)

-		%email% = (new user) Ahora si. Volvamos en que puedo ayudarte  			POSTBACK(servicio=initservice,producto=initproduct,horario=inithour)



*/