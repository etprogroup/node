var express = require('express');
var passport = require('passport');
var router = express.Router();
var cors = require('cors');

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);
var mongoose = require('mongoose');
var db = require(libs + 'db/mongoose');
var Order = require(libs + 'model/order');
var Shop = require(libs + 'model/shop');
var Notifications = require(libs + 'modules/notifications');
var Product = require(libs + 'model/product');

router.get('/', passport.authenticate('bearer', { session: false }), function(req, res) {

    var search = {}
    if(req.query.status){
        search.orderStatusChange = {
                $elemMatch: {
                    status: req.query.status
                }
            }
    }

    if (req.query.orderType) {
        search.orderType = req.query.orderType ;
    }

    if (req.query.dateIni && req.query.dateEnd) {
        search.created = { "$gte": new Date(req.query.dateIni), "$lte": new Date(req.query.dateEnd) };
    }


    if(req.query.shops){
        var list = req.query.shops.split(",");
        var shopIds = [];
        list.forEach(function(shop){
            shopIds.push( new mongoose.Types.ObjectId( shop ) );
        });
        search.shop = {$exists:true , "$in": shopIds};
    }
    if(req.query.brand){
        var list = req.query.brand;
        var brandIds = list.split(",");
    }

    Order.find(search, function (err, Orders) {
        if (!err) {
            var orderFiltered = Orders;
            if(brandIds && brandIds.length > 0){
                if(Orders.length){
                    orderFiltered = []
                    Orders.map(function (order) {
                        var list = order.orderList;
                        if(list.length){
                            list.map(function (service_order) {
                                if (service_order.product){
                                    if(service_order.product.brand && service_order.product.brand._id){
                                        var index = brandIds.indexOf(service_order.product.brand.id)
                                        if(index != -1){
                                            orderFiltered.push(order)
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            }
            return res.json(orderFiltered);
        } else {
            res.statusCode = 500;

            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    Order.findById(req.params.id, function (err, Order) {

        if(!Order) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK',
                Order: Order
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

router.get('/shop/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var shopId = req.params.id;
    var search = { 'shop': shopId };
    
    
    if(req.query.dateIni) {
        var startDate = new Date(req.query.dateIni)
        startDate.setHours(0);
        if (!req.query.dateEnd)
            req.query.dateEnd = req.query.dateIni;
        var endDate = new Date(req.query.dateEnd);
        endDate.setHours(23);
        search.created = { "$gte": startDate, "$lte": endDate };
    }
            
    Order.find( search , function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});


router.get('/orderListByShop/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var shopId = req.params.id;
    var search = { 'shop': shopId };


    if(req.query.dateIni) {
        var startDate = new Date(req.query.dateIni)
        startDate.setHours(0);
        if (!req.query.dateEnd)
            req.query.dateEnd = req.query.dateIni;
        var endDate = new Date(req.query.dateEnd);
        endDate.setHours(23);
        search.created = { "$gte": startDate, "$lte": endDate };
    }

    Order.find( search , function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            var result = [];
            docs.map(function (v) {
                var order = {
                    _id : v._id ? v._id : null,
                    shopName : v.shop ? v.shop.settings.name : "",
                    orderStatusChange : v.orderStatusChange,
                    orderType : v.orderType ? v.orderType : "",
                    customerName : v.customer ? v.customer.surname ? v.customer.name + ' ' + v.customer.surname : v.customer.name : "",
                    created : v.created,
                    orderList : v.orderList,
                    photoPublishAuth : v.photoPublishAuth,
                    totalAmount : v.totalAmount
                }
                result.push(order)
            })
            return res.json(result);
        }
    });
});

router.get('/customer/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    var customerId = req.params.id;

    Order.find({ 'customer': customerId }, function (err, docs) {
        if (err) {
            res.statusCode = 500;
            log.error('Customer with id: %s Not Found', customerId);
            return res.json({
                error: 'Not found',
                fullError: err
            });
        } else {
            return res.json(docs);
        }
    });
});

router.options('/:id', cors());

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    var shopId = req.params.id;

    Order.findById(shopId, function (err, Order) {
        if(!Order) {
            res.statusCode = 404;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found'
            });
        }
//TODO:Change Order parameters
        Order.source = req.body.source || Order.source;
        Order.customer = req.body.customer || Order.customer;
        Order.appointment = req.body.appointment || Order.appointment;
        Order.orderType = req.body.orderType || Order.orderType;
        Order.totalAmount = req.body.totalAmount || Order.totalAmount;
        Order.orderList= req.body.orderList || Order.orderList;
        Order.orderStatusChange= req.body.orderStatusChange || Order.orderStatusChange;
        Order.lastModified= Date.now();

        Order.save(function (err) {
            if (!err) {
                log.info("Order with id: %s updated, sending mail", Order.id);    
            
                return res.json({
                    status: 'OK',
                    Order: Order
                });
            } else {
                if(err.name === 'ValidationError') {
                    res.statusCode = 400;
                    return res.json({
                        error: 'Validation error',
                        fullError: err
                    });
                } else {
                    res.statusCode = 500;
                    log.error('Internal error (%d): %s', res.statusCode, err.message);

                    return res.json({
                        error: 'Server error'
                    });
                }
            }
        });
    });
});

router.post('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    var shopId = req.body.shop;

    Shop.findById(shopId, function (err, shop) {
        if(!shop) {
            res.statusCode = 404;
            log.error('Shop with id: %s Not Found', shopId);
            return res.json({
                error: 'Not found'
            });
        }

        var newOrder = new Order({
            shop: shopId,
            source : req.body.source,
            customer : req.body.customer,
            appointment : req.body.appointment,
            orderType : req.body.orderType,
            totalAmount : req.body.totalAmount,
            orderList: req.body.orderList,
            orderStatusChange: req.body.orderStatusChange,
            photoPublishAuth: req.body.photoPublishAuth
        });

        newOrder.save(function (err) {
            if (!err) {
                log.info("Order with id: %s inserted. OrderType %s.", newOrder.id, newOrder.orderType);

                if(newOrder.orderType == 'Buy_Product_MAQ'){
                    Notifications.notifyOrdertoAdmin(shopId)
                }
                else if(newOrder.orderType == 'Sell_Product_Shop' || newOrder.orderType == 'Sell_Product_MAQ'){
                    //update stock of products
                    var orderList = newOrder.orderList
                    orderList.map(function (prod) {
                        var productId = prod.product
                        var quantity = prod.quantity
                        updateStockProduct(productId, quantity)
                    })
                }

                return res.json({
                    status: 'OK',
                    Order: newOrder
                });
            } else {
                if(err.name === 'ValidationError') {
                    res.statusCode = 400;
                    return res.json({
                        error: 'Validation error!',
                        fullError: err
                    });
                } else {
                    res.statusCode = 500;
                    console.error('Internal error (%d): %s', res.statusCode, err.message);

                    return res.json({
                        error: 'Server error',
                        fullError: err
                    });
                }
            }
        });
    });
});



router.delete('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

    Order.findByIdAndRemove(req.params.id, function (err, Order) {

        if(!Order) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK'
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

function updateStockProduct(productId, quantity){

    Product.findById(productId, function (err, product) {
        if (!product) {
            log.error('Product with id: %s Not Found', productId);
        }
        else{
            product.stock = product.stock - quantity
            product.save(function (err) {
                if (!err) {
                    log.info("Product with id: %s updated", product.id)
                    log.info(product.name)
                } else {
                    log.error("Error")
                    log.error(err.name)
                    log.error(err.message)
                }
            });

        }
    });
}

module.exports = router;
