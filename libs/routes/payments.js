var express = require('express');
var passport = require('passport');
var router = express.Router();
var cors = require('cors');
var crypto = require('crypto');

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var Customer = require(libs + 'model/customer');
var Product = require(libs + 'model/product');
var Order = require(libs + 'model/order');
var https = require('https');
var parseString = require('xml2js').parseString;
var optionsPayments = {
    host: 'remote.addonpayments.com',
    path: '/remote',
    method: 'POST'
};

function randomStr() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 20; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function getFormattedTime(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var min = today.getMinutes();
    var hh = today.getHours();
    var ss = today.getSeconds();

    if(dd<10)dd='0'+dd;
    if(mm<10)mm='0'+mm;
    if(min<10)min='0'+min;
    if(hh<10)hh='0'+hh;
    if(ss<10)ss='0'+ss;

    return yyyy+mm+dd+hh+min+ss;
}

function saveOrder(order,cb){
    order.save(function (err) {
        if (!err) {
            console.log("Order with id: %s inserted.", order.id, order.email);
            if(cb)cb()
        } else {
            if(err.name === 'ValidationError') {
                res.statusCode = 400;
                console.error(err)
            } else {
                res.statusCode = 500;
                console.error('Internal error (%d): %s', res.statusCode, err);
            }
        }
    });
}

router.post('/payer/', passport.authenticate('bearer', { session: false }), function (req, res) {

    var customer_id = req.body.customer;

    Customer.findOne({ '_id': customer_id }, function (err, customer) {
            if (err) {
                res.status(500).send(err);
            } else if(!customer) {
                res.statusCode = 404;
                log.error('Customer with id: %s Not Found');
                return res.json({
                    error: 'Customer not found'
                });
            }else if(
                customer.shippingAddress[0].address
                && customer.shippingAddress[0].city
                && customer.shippingAddress[0].state
                && customer.shippingAddress[0].postalCode
            ){

                var todayFormatted = getFormattedTime();
                var orderId = randomStr();

                var stringPre = todayFormatted+".magnifique."+orderId+"..."+customer_id;
                var hashPre = crypto.createHash('sha1').update(stringPre).digest('hex');
                var hash = crypto.createHash('sha1').update(hashPre+'.FSYKV35sus').digest('hex');
                var title = customer.sex==='Hombre' ? 'Sr.' : 'Sra.';
                var type = customer.payerSaved==='true' ? 'payer-edit' : 'payer-new';

                var body="<?xml version='1.0' encoding='UTF-8'?>"+
                    "<request type='"+type+"' timestamp='"+todayFormatted+"'>"+
                    "<merchantid>magnifique</merchantid>"+
                    "<account>internet</account>"+
                    "<orderid>"+orderId+"</orderid>"+
                    "<payer ref='"+customer_id+"' type='Retail'>"+
                    "    <title>"+title+"</title>"+
                    "    <firstname>"+customer.name+"</firstname>"+
                    "    <surname>"+customer.surname+"</surname>"+
                    "    <address>"+
                    "       <line1>"+customer.shippingAddress[0].address+"</line1>"+
                    "       <city>"+customer.shippingAddress[0].city+"</city>"+
                    "       <county>"+customer.shippingAddress[0].state+"</county>"+
                    "       <postcode>"+customer.shippingAddress[0].postalCode+"</postcode>"+
                    "       <country code='ES'>EspaÃƒÂ±a</country>"+
                    "    </address>"+
                    "    <phonenumbers>"+
                    "       <mobile>"+customer.phoneNumber+"</mobile>"+
                    "    </phonenumbers>"+
                    "    <email>"+customer.email+"</email>"+
                    "</payer>"+
                    "<sha1hash>"+hash+"</sha1hash>"+
                    "</request>";

                var callback = function(response) {
                    var str = ''
                    response.on('data', function (chunk) {
                        str += chunk;
                    });

                    response.on('end', function () {
                        //console.log(str);
                        parseString(str, function (err, result) {
                            if(result.response.result[0]==='00'){
                                customer.payerSaved='true';
                                customer.save(function (err, employee) {
                                    if (!err) {
                                        log.info("Customer with id: %s modified", employee.id);

                                    } else {
                                        log.error('Internal error (%d): %s', res.statusCode, err);
                                    }
                                });
                                res.json({
                                    message:'OK'
                                })
                            }
                        })
                    });
                    response.on('error', (e) => {
                        //console.error(e);
                        res.json({
                            error:e
                        })
                    });
                }

                var req2 = https.request(optionsPayments, callback);
                req2.write(body);
                req2.end();
            }
        }
    );
});

router.post('/card/', passport.authenticate('bearer', { session: false }), function (req, res) {

    var customer_id = req.body.customer;
    var number = req.body.cardNumber;
    var expiration = req.body.expiration;
    var name = req.body.name;

    Customer.findOne({ '_id': customer_id }, function (err, customer) {
            if (err) {
                res.status(500).send(err);
            } else if(!customer) {
                res.statusCode = 404;
                log.error('Customer with id: %s Not Found');
                return res.json({
                    error: 'Customer not found'
                });
            }else if(
                number
                && expiration
                && name
            ){

                var todayFormatted = getFormattedTime();
                var orderId = randomStr();


                //NEW "timestamp.merchantid.orderid.amount.currency.payerref.chname.cardnumber"
                var stringPre = todayFormatted+".magnifique."+orderId+"..."+customer_id+'.'+name+'.'+number;
                var hashPre = crypto.createHash('sha1').update(stringPre).digest('hex');
                var hash = crypto.createHash('sha1').update(hashPre+'.FSYKV35sus').digest('hex');
                var type = 'card-new';

                if(customer.cardSaved==='true'){
                    //UPDATE "timestamp.merchantid.payerref.cardref.expirydate.cardnumber"
                    var stringPre = todayFormatted+".magnifique."+customer_id+".main."+expiration+"."+number;
                    var hashPre = crypto.createHash('sha1').update(stringPre).digest('hex');
                    var hash = crypto.createHash('sha1').update(hashPre+'.FSYKV35sus').digest('hex');
                    var type = 'card-update-card';
                }


                var body="<?xml version='1.0' encoding='UTF-8'?>"+
                    "<request type='"+type+"' timestamp='"+todayFormatted+"'>"+
                    "<merchantid>magnifique</merchantid>"+
                    "<account>internet</account>"+
                    "<orderid>"+orderId+"</orderid>"+
                    "<card>"+
                    "    <ref>main</ref>"+
                    "    <payerref>"+customer_id+"</payerref>"+
                    "    <number>"+number+"</number>"+
                    "    <expdate>"+expiration+"</expdate>"+
                    "    <chname>"+name+"</chname>"+
                    "    <type>VISA</type>"+
                    "</card>"+
                    "<sha1hash>"+hash+"</sha1hash>"+
                    "</request>";

                //The url we want is `www.nodejitsu.com:1337/`

                var callback = function(response) {
                    var str = '';
                    response.on('data', function (chunk) {
                        str += chunk;
                    });

                    response.on('end', function () {
                        log.info(str);
                        parseString(str, function (err, result) {
                            if(result.response.result[0]==='00'){
                                customer.cardSaved='true';
                                customer.save(function (err2, employee) {
                                    if (!err2) {
                                        log.info("Customer with id: %s modified", employee.id);

                                    } else {
                                        log.error('Internal error (%d): %s', res.statusCode, err2);
                                    }
                                });
                                res.json({
                                    message:'OK'
                                })
                            }
                        })
                    });
                    response.on('error', (e) => {
                        //console.error(e);
                        res.json({
                            error:e
                        })
                    });
                }

                var req2 = https.request(optionsPayments, callback);
                req2.write(body);
                req2.end();
            }else{
                res.json({
                    message:'Not enough data'
                })
            }
        }
    );
});

router.post('/checkout/', passport.authenticate('bearer', { session: false }), function (req, res) {

    var customer_id = req.body.customer;
    var amount = req.body.amount;
    var products = [req.body.product];
    var firstPayment = req.body.firstPayment ? firstPayment : false;

    Customer.findOne({ '_id': customer_id }, function (err, customer) {
            if (err) {
                res.status(500).send(err);
            } else if(!customer) {
                res.statusCode = 404;
                log.error('Customer with id: %s Not Found');
                return res.json({
                    error: 'Customer not found'
                });
            }else{

                var newOrder = new Order({
                    shop: customer.shop,
                    customer : customer_id,
                    appointment : req.body.appointment,
                    orderType : req.body.orderType,
                    totalAmount : amount,
                    orderList: products,
                    orderStatusChange: [{
                        status:5,
                        created:Date.now(),
                        comments:'Creado en back antes de pagar'
                    }]
                });

                var todayFormatted = getFormattedTime();
                var orderId = newOrder._id; //randomStr();

                var stringPre = todayFormatted+".magnifique."+orderId+"."+amount+".EUR."+customer_id;
                var hashPre = crypto.createHash('sha1').update(stringPre).digest('hex');
                var hash = crypto.createHash('sha1').update(hashPre+'.FSYKV35sus').digest('hex');

                saveOrder(newOrder,requestPayment);

                var body="<?xml version='1.0' encoding='UTF-8'?>"+
                    "<request type='receipt-in' timestamp='"+todayFormatted+"'>"+
                    "<merchantid>magnifique</merchantid>"+
                    "<account>internet</account>"+
                    "<channel>ECOM</channel>"+
                    "<orderid>"+orderId+"</orderid>"+
                    "<amount currency='EUR'>"+amount+"</amount>"+
                    "<autosettle flag='1'/>"+
                    "<payerref>"+customer_id+"</payerref>"+
                    "<paymentmethod>main</paymentmethod>"+
                    "<paymentdata>"+
                    "   <cvn>"+
                    "       <presind>4</presind>"+
                    "   </cvn>"+
                    "</paymentdata>"+
                    "<sha1hash>"+hash+"</sha1hash>"+
                    "</request>";

                if(firstPayment!==false){
                    body="<?xml version='1.0' encoding='UTF-8'?>"+
                        "<request type='realvault-3ds-verifyenrolled' timestamp='"+todayFormatted+"'>"+
                        "<merchantid>magnifique</merchantid>"+
                        "<account>internet</account>"+
                        "<orderid>"+orderId+"</orderid>"+
                        "<amount currency='EUR'>"+amount+"</amount>"+
                        "<payerref>"+customer_id+"</payerref>"+
                        "<paymentmethod>main</paymentmethod>"+
                        "<sha1hash>"+hash+"</sha1hash>"+
                        "</request>";
                }

                var callback = function(response) {
                    var str = '';
                    response.on('data', function (chunk) {
                        str += chunk;
                    });

                    response.on('end', function () {
                        log.info(str);

                        parseString(str, function (err, result) {
                            //We check if payment is OK

                            if(result.response.result[0]==='00'){
                                newOrder.orderStatusChange.push({
                                    status:10,
                                    created:Date.now(),
                                    comments:'Pagado por pasarela Caixa'
                                });
                                saveOrder(newOrder);

                                res.json({
                                    message:'OK',
                                    respose:result.response
                                })

                            }else{
                                res.json({
                                    message:'error',
                                    firstPayment:firstPayment,
                                    respose:result.response,
                                    error:result.response.result
                                })
                            }

                        });
                    });
                    response.on('error', (e) => {
                        //console.error(e);

                        res.json({
                            error:e
                        })
                    });
                }

                function requestPayment(){
                    var req2 = https.request(optionsPayments, callback);
                    req2.write(body);
                    req2.end();
                }
            }
        }
    );
});

router.post('/process3dSecure/', passport.authenticate('bearer', { session: false }), function (req, res) {

    var pares = req.body.pares;
    var amount = req.body.amount;
    var orderId = req.body.orderId;

    var todayFormatted = getFormattedTime();
    var stringPre = todayFormatted+".magnifique."+orderId+"."+amount+".EUR.";
    var hashPre = crypto.createHash('sha1').update(stringPre).digest('hex');
    var hash = crypto.createHash('sha1').update(hashPre+'.FSYKV35sus').digest('hex');

    var body="<?xml version='1.0' encoding='UTF-8'?>"+
        "<request type='3ds-verifysig' timestamp='"+todayFormatted+"'>"+//realvault-
        "<merchantid>magnifique</merchantid>"+
        "<account>internet</account>"+
        "<orderid>"+orderId+"</orderid>"+
        "<amount currency='EUR'>"+amount+"</amount>"+
        "<pares>"+pares+"</pares>"+
        "<sha1hash>"+hash+"</sha1hash>"+
        "</request>";

    var callback = function(response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });

        response.on('end', function () {
            log.info(str);

            parseString(str, function (err, result) {
                //We check if payment is OK

                if(result.response.result[0]==='00'){

                    res.json({
                        message:'OK',
                        respose:result.response
                    })

                }else{
                    res.json({
                        message:'error',
                        respose:result.response,
                        error:result.response.result
                    })
                }

            });
        });

        response.on('error', (e) => {
            //console.error(e);

            res.json({
                error:e
            })
        });
    }


    var req2 = https.request(optionsPayments, callback);
    req2.write(body);
    req2.end();
});

router.post('/process3dSecure/checkout', passport.authenticate('bearer', { session: false }), function (req, res) {

    var amount = req.body.amount;
    var orderId = req.body.orderId;
    var status = req.body.status;
    var eci = req.body.eci;
    var xid = req.body.xid;
    var cavv = req.body.cavv;

    var todayFormatted = getFormattedTime();
    var stringPre = todayFormatted+".magnifique."+orderId+"."+amount+".EUR.";
    var hashPre = crypto.createHash('sha1').update(stringPre).digest('hex');
    var hash = crypto.createHash('sha1').update(hashPre+'.FSYKV35sus').digest('hex');

    var body="<?xml version='1.0' encoding='UTF-8'?>"+
        "<request type='auth' timestamp='"+todayFormatted+"'>"+
        "<merchantid>magnifique</merchantid>"+
        "<account>internet</account>"+
        "<orderid>"+orderId+"</orderid>"+
        "<amount currency='EUR'>"+amount+"</amount>"+
        "<autosettle flag='1'/>"+
        "<mpi>"+
        "<cavv>"+cavv+"</cavv>"+
        "<xid>"+xid+"</xid>"+
        "<eci>"+eci+"</eci>"+
        "</mpi>"+
        "<sha1hash>"+hash+"</sha1hash>"+
        "</request>";

    var callback = function(response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });

        response.on('end', function () {
            log.info(str);

            parseString(str, function (err, result) {
                //We check if payment is OK
                if(result.response.result[0]==='00'){

                    res.json({
                        message:'OK',
                        respose:result.response
                    })

                }else{
                    res.json({
                        message:'error',
                        respose:result.response,
                        error:result.response.result
                    })
                }

            });
        });

        response.on('error', (e) => {
            //console.error(e);

            res.json({
                error:e
            })
        });
    }


    var req2 = https.request(optionsPayments, callback);
    req2.write(body);
    req2.end();
});



router.get('/bank/process3dSecure/', function (req, res) {

    var html = '<script type="text/javascript">console.log("Api return!")</script>'+
        '<script type="text/javascript">'+
        'if(window.postMessage.length !== 1) {'+
        'window.postMessage = function(msg) {'+
        'setTimeout(function () {'+
        'window.postMessage(msg);'+
        '}, 500);'+
        '}'+
        '}'+
        "window.postMessage('"+JSON.stringify(req.params)+"','*');"+
        '</script>';

    res.set('Content-Type', 'text/html');
    res.send(new Buffer(html));
    return res;

    /*
    return res.json({
                req:req.params,
                detail:'get',
                script:"<script>console.log('messageGET')</script>"
            });

  */
});

router.post('/bank/process3dSecure/', function (req, res) {

    var html = '<script type="text/javascript">console.log("Api return!")</script>'+
        '<script type="text/javascript">'+
        'if(window.postMessage.length !== 1) {'+
        'window.postMessage = function(msg) {'+
        'setTimeout(function () {'+
        'window.postMessage(msg);'+
        '}, 500);'+
        '}'+
        '}'+
        "window.postMessage('"+JSON.stringify(req.body)+"','*');"+
        '</script>';

    res.set('Content-Type', 'text/html');
    res.send(new Buffer(html));
    return res;
    /*
        return res.json({
                    req:req.body,
                    detail:'post',
                    script:"<script>console.log('messagePost')</script>"
                });
    */
});

router.post('/checkout/tpv/', passport.authenticate('bearer', { session: false }), function (req, res) {

    var customerId = req.body.customer;
    var amount = req.body.amount;
    var products = [req.body.product];

    Customer.findOne({ '_id': customerId }, function (err, customer) {
            if (err) {
                res.status(500).send(err);
            } else if(!customer) {
                res.statusCode = 404;
                log.error('Customer with id: %s Not Found');
                return res.json({
                    error: 'Customer not found'
                });
            }else{


                var newOrder = new Order({
                    shop: customer.shop,
                    customer : customerId,
                    appointment : req.body.appointment,
                    orderType : req.body.orderType,
                    totalAmount : amount,
                    orderList: products,
                    orderStatusChange: [{
                        status:5,
                        created:Date.now(),
                        comments:'Creado en back antes de pagar'
                    }]
                });

                var merchantId = 'magnifique';
                var currency = 'EUR';
                var todayFormatted = getFormattedTime();
                var orderId = newOrder._id; //randomStr();

                var url = 'https://hpp.addonpayments.com/pay';
                //url = 'https://hpp.sandbox.addonpayments.com/pay';
                var termUrl = req.body.termUrl;

                //"timestamp.merchantid.orderid.amount.currency"
                var stringPre = todayFormatted+"."+merchantId+"."+orderId+"."+amount+"."+currency;
                var hashPre = crypto.createHash('sha1').update(stringPre).digest('hex');
                var hash = crypto.createHash('sha1').update(hashPre+'.FSYKV35sus').digest('hex');

                saveOrder(newOrder,callback);


                var html = '<form id="securePayment" action="'+url+'" method="post">'+
                    '<input type="hidden" name="TIMESTAMP" value="'+todayFormatted+'"/>'+
                    '<input type="hidden" name="MERCHANT_ID" value="'+merchantId+'"/>'+
                    '<input type="hidden" name="ACCOUNT" value="internet"/>'+
                    '<input type="hidden" name="ORDER_ID" value="'+orderId+'"/>'+
                    '<input type="hidden" name="AMOUNT" value="'+amount+'"/>'+
                    '<input type="hidden" name="CURRENCY" value="'+currency+'"/>'+
                    '<input type="hidden" name="SHA1HASH" value="'+hash+'"/>'+
                    '<input type="hidden" name="AUTO_SETTLE_FLAG" value="1"/>'+
                    '<input type="hidden" name="COMMENT1" value="Customer '+customerId+'"/>'+
                    '<input type="hidden" name="COMMENT2" value="Product '+JSON.stringify(products)+'"/>'+
                    '<input type="hidden" name="SHIPPING_CODE" value="E77|4QJ"/>'+
                    '<input type="hidden" name="SHIPPING_CO" value="ES"/>'+
                    '<input type="hidden" name="BILLING_CODE" value="R90|ZQ7"/>'+
                    '<input type="hidden" name="BILLING_CO" value="ES"/>'+
                    '<input type="hidden" name="CUST_NUM" value="'+customerId+'"/>'+
                    '<input type="hidden" name="VAR_REF" value="Invoice '+todayFormatted+'"/>'+
                    '<input type="hidden" name="PROD_ID" value="'+products[0].product+'"/>'+
                    '<input type="hidden" name="HPP_LANG" value="ES"/>'+
                    '<input type="hidden" name="HPP_VERSION" value="2"/>'+
                    '<input type="hidden" name="MERCHANT_RESPONSE_URL" value="'+termUrl+'"/>'+
                    '<input type="hidden" name="CARD_PAYMENT_BUTTON" value="Pagar ahora"/>'+
                    '<input type="hidden" name="SUPPLEMENTARY_DATA" value="Valor personalizado"/>'+
                    //'<button type="submit">send</button>'+
                    '</form>'+
                    '<script type="text/javascript">'+
                    'document.getElementById("securePayment").submit();'+
                    '</script>';


                var key = crypto.createCipher('aes-256-ctr', 'formPayments');
                var value = key.update(html, 'utf8', 'hex');
                //value += key.update.final('hex');

                var urlHtml = 'https://magnifique.me/api/payments/checkout/tpv/print';
                //urlHtml = 'http://dev.magnifique.me/api/payments/checkout/tpv/print';

                var newHtml = '<form id="securePayment" action="'+urlHtml+'" method="post">'+
                    '<input type="hidden" name="html" value="'+value+'"/>'+
                    '</form>'+
                    '<script type="text/javascript">'+
                    'document.getElementById("securePayment").submit();'+
                    '</script>';

                function callback(response) {
                    return res.json({
                        html: newHtml,
                        orderId: orderId,
                        order: newOrder,
                        response: response
                    });
                }
            }
        }
    );
});

router.post('/checkout/tpv/print/', function (req, res) {
    var html = req.body.html;

    var key = crypto.createDecipher('aes-256-ctr', 'formPayments');
    var form = key.update(html, 'hex', 'utf8');
    //form += key.update.final('utf8');

    res.set('Content-Type', 'text/html');
    res.send(new Buffer(form));
    return res;
});


router.post('/checkout/tpv/response/', passport.authenticate('bearer', { session: false }), function (req, res) {

    var orderId = req.body.orderId;

    Order.findOne({ '_id': orderId }, function (err, order) {
            if (err) {
                res.status(500).send(err);
            } else if(!order) {
                res.statusCode = 404;
                log.error('Order with id: %s Not Found');
                return res.json({
                    error: 'Order not found'
                });

            }else{

                order.orderList.map(function (prod) {
                    var productId = prod.product
                    var quantity = prod.quantity
                    updateStockProduct(productId, quantity)
                })

                order.orderStatusChange.push({
                    status:10,
                    created:Date.now(),
                    comments:'Pagado por pasarela Caixa'
                });
                saveOrder(order);


                return res.json({
                    result: 'ok',
                    message: 'Pago realizado correctamente'
                });
            }
        }
    );
});

//convert to module
function updateStockProduct(productId, quantity){

    Product.findById(productId, function (err, product) {
        if (!product) {
            log.error('Product with id: %s Not Found', productId);
        }
        else{
            product.stock = product.stock - quantity
            product.save(function (err) {
                if (!err) {
                    log.info("Product with id: %s updated", product.id)
                    log.info(product.name)
                } else {
                    log.error("Error")
                    log.error(err.name)
                    log.error(err.message)
                }
            });

        }
    });
}

module.exports = router;
