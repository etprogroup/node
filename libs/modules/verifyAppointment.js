var express = require('express');
var passport = require('passport');
var router = express.Router();
var cors = require('cors');

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var Moment = require('moment');
var mongoose = require('mongoose');
var Appointment = require(libs + 'model/appointment');
var Shop = require(libs + 'model/shop');
var Notifications = require(libs + 'modules/notifications');
var Employee = require(libs + 'model/employee');
var Service = require(libs + 'model/service');
var async = require('async');

let verifyAppointment = {};

verifyAppointment.verifyNextAppointment = async function(appointment, useFrequency){
    var employeeId = appointment.employee;
    var employee = null;
    var result = null;

    //EMPLOYEE
    await Employee.findById(employeeId, function (err, data) {
        if (!err) {
            if(data){
                employee = data;

            }else{
                result = {
                    error: 'Not found',
                    fullError: err
                };
            }

        }else{
            log.error('Error getting Employee with id: %s ', employeeId);
            //res.status(500).send(err);
            result = {
                error: 'Not found',
                fullError: err
            };
        }
    });

    result = {
        type: "employee",
        employee: employee,
    };

    //SERVICES
    var formIniTime = new Date(appointment.start);
    var serviceList = appointment.services;
    var search = { '_id': { $in: serviceList } }
    var services = await Service.find( search , async function (err, data) {
        if (err) {
            //res.statusCode = 500;
            log.error('Appointments with id: %s Not Found', serviceList);

            result = {
                error: 'Not found',
                fullError: err
            };

        }else {
            services = data;

        }
    });

    result = {
        type: "service",
        services: services,
    };

    if(services && services.length > 0){
        //calcular  fecha de la proxima cita
        var totalDuration=0;
        services.forEach(function(value){
            totalDuration += value.duration;
        });
        var frequency = services.map(function (value){return value.frequency});
        var minFrequency = Math.min(...frequency);

        if(useFrequency!==false){
            formIniTime.setDate(formIniTime.getDate() + minFrequency);
        }
    }

    //APPOINTMENTS
    var appointments = await verifyAppointment.getAppointmentsByEmployee(employee, async function(appointments){});

    result = {
        type: "appointments",
        appointments: appointments,
    };

    var offset1 = Moment().utcOffset();
    var formIniTimeString = Moment.utc(formIniTime).utcOffset(offset1).format('YYYY-MM-DD');
    var list = appointments.filter(function (appointment) {
        return appointment.start.includes(formIniTimeString)
    })
    log.info("list")
    log.info(list.length)

    // si shop y employee no esta de vacaciones ese dia y en su horario
    var isCorrect = verifyAppointment.isFreeDate(employee,formIniTime, list)

    var formEndTime = new Date(appointment.start);
    formEndTime.setDate(formEndTime.getDate() + minFrequency);
    formEndTime.setMinutes(formEndTime.getMinutes() + totalDuration);
    var maxEventDate = verifyAppointment.getNextEventStartTime(formIniTime, list)
    var isCompressedDuration = false
    var diffTime = 0
    var endTimediff = verifyAppointment.getMinutesDiff(maxEventDate,formEndTime);
    if(endTimediff<0){
        isCompressedDuration = true
        formEndTime=new Date(maxEventDate);
        diffTime = Math.round(Math.abs(endTimediff))
    }
    while(!isCorrect){
        formIniTime.setDate(formIniTime.getDate() + 1);
        formEndTime.setDate(formIniTime.getDate());
        var offset = Moment().utcOffset();
        var timeString = Moment.utc(formIniTime).utcOffset(offset).format('YYYY-MM-DD');
        list = appointments.filter(function (appointment) {
            return appointment.start.includes(timeString)
        })
        log.info("list1")
        log.info(list.length)
        var maxEventDate = verifyAppointment.getNextEventStartTime(formIniTime, list)
        var isCompressedDuration = false
        var diffTime = 0
        var endTimediff = verifyAppointment.getMinutesDiff(maxEventDate,formEndTime);
        if(endTimediff<0){
            isCompressedDuration = true
            formEndTime=new Date(maxEventDate);
            diffTime = Math.round(Math.abs(endTimediff))
        }
        isCorrect = verifyAppointment.isFreeDate(employee, formIniTime,list)
    }

    result =  {
        status: 'OK',
        formIniTime: formIniTime,
        formEndTime: formEndTime,
        isCorrect: isCorrect,
        isCompressedDuration: isCompressedDuration,
        diffTime: diffTime
    };


    return result;
}



verifyAppointment.isFreeDate = async function(employee, start, appointments){
    log.info("isFreeDate")
    var shopHolidays = employee.shop ? employee.shop.holidays : [];
    //var opening = employee.shop ? employee.shop.opening : [];
    var employeeHolidays = employee.holidays ? employee.holidays : [];

    var offset = Moment().utcOffset();
    var calendarDayFormatted = Moment.utc(start).utcOffset(offset).format('DD-MM-YYYY');
    var isOutShopHolidays = shopHolidays ? shopHolidays.indexOf(calendarDayFormatted) === -1 : true
    var isOutEmployeeHolidays = employeeHolidays ? employeeHolidays.indexOf(calendarDayFormatted) === -1 : true

    if(appointments && appointments.length > 0){
        log.info(appointments.length)
        var isOutOfAppointments = true
        appointments.forEach(function(appointment){
            var startDate = new Date(appointment.start)
            var hour = startDate.getHours()
            var minutes = startDate.getMinutes()
            log.info(start)
            log.info(start.getHours())
            log.info(start.getMinutes())
            log.info(appointment.start)
            log.info(hour)
            log.info(minutes)
            if(start.getHours() === hour && (start.getMinutes() === minutes || start.getMinutes() > minutes)){
                log.info("horas q coinciden")
                log.info(hour)
                log.info("minutos q coinciden")
                log.info(minutes)
                isOutOfAppointments = false;
            }
        })
    }
    else
        var isOutOfAppointments = true;
    return isOutShopHolidays && isOutEmployeeHolidays && isOutOfAppointments
}

verifyAppointment.getAppointmentsByEmployee =  async function(employee, fn){
    log.info("buscando las citas")
    log.info(employee.name)
    var appointments = [];
    var result = [];

    await Appointment.find({'shop': employee.shop._id, 'employee': employee._id}, async function(error, data) {
        appointments = data;
    });

    log.info("getAppointmentsByEmployee..");
    log.info(appointments.length);
    if(appointments && appointments.length > 0){
        result = await appointments.filter(async function(appointment){
            return (appointment.status!=='Cancelado' && appointment.status!=='Eliminado' && appointment.status!=='Pagado')
        })
    }
    log.info("length del resultado")
    log.info(result.length)
    fn(result);
    return result;
}

verifyAppointment.getMinutesDiff = async function(endDate,iniDate){
    return (
        Moment
            .utc(Moment(new Date(endDate),"DD/MM/YYYY HH:mm:ss")
                .diff(Moment(new Date(iniDate),"DD/MM/YYYY HH:mm:ss"))
            )
            .valueOf())/(1000*60)
}

verifyAppointment.getNextEventStartTime = async function(iniTime,appointments){
    var nextEvent=new Date(2100,1,1);
    var minDiff=10000;
    if(appointments){
        appointments.map(async function(event){
            var diff= await verifyAppointment.getMinutesDiff(event.start,iniTime);
            if(diff<minDiff && diff>0){
                minDiff=diff;
                nextEvent=event;
            }
        })
    }
    return nextEvent.start;
}


module.exports = verifyAppointment;